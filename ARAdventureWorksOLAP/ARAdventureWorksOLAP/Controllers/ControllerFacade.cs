﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ARCAPI;
using ARCCore;

namespace ARAdventureWorksOLAP.Controllers {

    [Class(Description =
        "Facade for entry into the desired -" + nameof(ARCAPI.BaseController) + "-.\r\n"
    )]
    [ApiController]
    public class ControllerFacade : ControllerBase {

        private static long _countConcurrentRequests = 0;

        [HttpGet]
        [Route("{**request}")]
        [ClassMember(
            Description =
                "Catches all HTTP GET requests to this API.\r\n" +
                "\r\n" +
                "Calls -" + nameof(BaseController.CatchAll) + "- which again chooses correct controller to use in order to serve request.\r\n" +
                "\r\n" +
                "Translates generated result into a Microsoft.AspNetCore.Mvc.ContentResult instance.\r\n" +
                "\r\n" +
                "Note that you can add additional endpoints to your API, outside of this standard AgoRapide mechanism.\r\n"
        )]
        public ActionResult CatchAll(string? request) {
            if (string.IsNullOrEmpty(request)) {
                // This is not possible as it would break relative links (browser and ARCore would have different understanding of "current" location)
                // request = "RQ/";
                //
                // Instead we have to return a redirect result like this:
                return Redirect("RQ/");
            }

            try {
                // Limit to three concurrent requests at any one time.
                if (System.Threading.Interlocked.Increment(ref _countConcurrentRequests) > 3) {
                    return TooManyRequestsResponse;
                }
                return BaseController.CatchAll(strRequest: request, postData: null, dataStorage: Program.DataStorage).Use(t => new Microsoft.AspNetCore.Mvc.ContentResult {
                    StatusCode = (int)t.StatusCode,
                    ContentType = t.ContentType,
                    Content = t.Content
                });
            } finally {
                System.Threading.Interlocked.Decrement(ref _countConcurrentRequests);
            }

        }

        [HttpPost]
        [Route("{**request}")]
        [ClassMember(
            Description =
                "Catches all HTTP POST requests to this API.\r\n" +
                "\r\n" +
                "Calls -" + nameof(BaseController.CatchAll) + "- which again chooses correct controller to use in order to serve request.\r\n" +
                "\r\n" +
                "Note additional optional parameter postData (compared to catch all for HTTP GET.\r\n" +
                "\r\n" +
                "Translates generated result into a Microsoft.AspNetCore.Mvc.ContentResult instance.\r\n" +
                "\r\n" +
                "Note that you can add additional endpoints to your API, outside of this standard AgoRapide mechanism.\r\n"
        )]
        public ActionResult CatchAll(string? request, [FromForm][FromBody] string? postData) {
            try {
                // Limit to three concurrent requests at any one time.
                if (System.Threading.Interlocked.Increment(ref _countConcurrentRequests) > 3) {
                    return TooManyRequestsResponse;
                }
                return BaseController.CatchAll(strRequest: request, postData: postData, dataStorage: Program.DataStorage).Use(t => new Microsoft.AspNetCore.Mvc.ContentResult {
                    StatusCode = (int)t.StatusCode,
                    ContentType = t.ContentType,
                    Content = t.Content
                });
            } finally {
                System.Threading.Interlocked.Decrement(ref _countConcurrentRequests);
            }
        }

        public Microsoft.AspNetCore.Mvc.ContentResult TooManyRequestsResponse = new Microsoft.AspNetCore.Mvc.ContentResult {
            StatusCode = (int)System.Net.HttpStatusCode.TooManyRequests,
            ContentType = "text/html",
            Content =
                "<html><body>" +
                "<h1>" +
                    (int)System.Net.HttpStatusCode.TooManyRequests + ": " +
                    System.Net.HttpStatusCode.TooManyRequests +
                "</h1>" +
                "<p>" +
                    "Wow, fame at last!<br>" +
                    "<br>" +
                    "But unfortunately this is too much for this poor old creaky server.<br>" +
                    "There are just too many concurrent requests going on just now.<br>" +
                    "<br>" +
                    "Please try again later." +
                "</p>" +
                "</body></html>"
        };
    }
}