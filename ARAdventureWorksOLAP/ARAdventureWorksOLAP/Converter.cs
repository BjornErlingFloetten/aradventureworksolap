﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace ARAdventureWorksOLAP {

    [Class(Description =
        "Converts original sample database to property stream format.\r\n" +
        "\r\n" +
        "See -" + nameof(ConvertDatabase) + "-."
    )]
    public static class Converter {
        [ClassMember(Description =
            "Converts original sample database to property stream format if no data for 'DimCustomer' found.\r\n" +
            "\r\n" +
            "This method is only relevant for developers of ARAdventureWorksOLAP, if content of Data-folder is to be updated\r\n" +
            "(delete all content of Data-foler in order for this method to have any function).\r\n" +
            "\r\n" +
            "The parsing is quick-and-dirty and based on source files as it appeared in Feb 2021 (it had then not been changed since 2 Nov 2018).\r\n" +
            "\r\n" +
            "This code will normally not have to be run, since its output ( a -" + nameof(ARConcepts.PropertyStream) + "- representation of the sample database) " +
            "is contained in the git-repository of ARAdventureWorksOLAP anyway\r\n" +
            @"(in the folder ARAdventureWorksOLAP\ARAdventureWorksOLAP\ARAdventureWorksOLAP\Data)." + "\r\n" +
            "\r\n" +
            "Note: Code for converting -" + nameof(FactProductInventory) + "- has been commented out (see below). " +
            "Uncomment if you want to include that table also " +
            "(will take an additional 300 MB of disk space and corresponding more RAM).\r\n"
        )]
        public static void ConvertDatabase(StreamProcessor streamProcessor) {
            var key = nameof(DimCustomer);
            if (!Program.DataStorage.Storage[nameof(PSPrefix.dt)].ContainsKey(key)) {
                var logger = (IP)new PConcurrent();
                logger.Logger = s => streamProcessor.SendFromLocalOrigin(PSPrefix.app + "/" + Program.NodeId + "/" + nameof(Converter) + "/" + s);
                logger.LogKeyValue("Key", key);
                logger.LogText("Key not found in storage. Assuming that must perform conversion.");
                var slash = System.IO.Path.DirectorySeparatorChar;
                var r = new Func<int, string>(c => string.Join("", Enumerable.Repeat(slash + "..", c)));
                var cur = Environment.CurrentDirectory;
                var sourceFolder = cur + r(3) + slash + @"sql-server-samples\samples\databases\adventure-works\data-warehouse-install-script".Replace('\\', slash);
                sourceFolder = System.IO.Path.GetFullPath(sourceFolder);
                var resolution =
                    "Resolution:\r\nClone from\r\n" +
                    "https://github.com/microsoft/sql-server-samples \r\n" +
                    "and restart application.\r\n" +
                    "\r\n" +
                    "Place git-repository in parallell to\r\n" +
                    System.IO.Path.GetFullPath(Environment.CurrentDirectory + r(2)) + "\r\nas\r\n" +
                    System.IO.Path.GetFullPath(Environment.CurrentDirectory + r(3) + slash + "sql-server-samples") +
                    "\r\n\r\n";
                if (!System.IO.Directory.Exists(sourceFolder)) {
                    throw new AdventureWorksConversionException(
                        nameof(sourceFolder) + "\r\n" + sourceFolder + "\r\nnot found.\r\n" +
                        "\r\n" +
                        resolution
                    );
                }
                sourceFolder += slash;

                var files = new List<(string entityType, List<PK?> fields)> {
                    (
                    // Left out temporarily
                    //(
                    //    nameof(FactProductInventory),
                    //    new List<PK?> {
                    //        PK.FromEnum(FactProductInventoryP.DimProductId),
                    //        PK.FromEnum(FactProductInventoryP.Date),
                    //        PK.FromEnum(FactProductInventoryP.MovementDate),
                    //        PK.FromEnum(FactProductInventoryP.UnitCost),
                    //        PK.FromEnum(FactProductInventoryP.UnitsIn),
                    //        PK.FromEnum(FactProductInventoryP.UnitsOut),
                    //        PK.FromEnum(FactProductInventoryP.UnitsBalance)
                    //    }
                    //),
                        nameof(DimCustomer),
                        new List<PK?> {
                            PK.FromEnum(DimCustomerP.DimGeographyId),
                            PK.FromEnum(DimCustomerP.AlternateKey),
                            PK.FromEnum(DimCustomerP.Title),
                            PK.FromEnum(DimCustomerP.FirstName),
                            PK.FromEnum(DimCustomerP.MiddleName),
                            PK.FromEnum(DimCustomerP.LastName),
                            PK.FromEnum(DimCustomerP.NameStyle),
                            PK.FromEnum(DimCustomerP.BirthDate),
                            PK.FromEnum(DimCustomerP.MaritalStatus),
                            PK.FromEnum(DimCustomerP.Suffix),
                            PK.FromEnum(DimCustomerP.Gender),
                            PK.FromEnum(DimCustomerP.EmailAddress),
                            PK.FromEnum(DimCustomerP.YearlyIncome),
                            PK.FromEnum(DimCustomerP.TotalChildren),
                            PK.FromEnum(DimCustomerP.NumberChildrenAtHome),
                            PK.FromEnum(DimCustomerP.EnglishEducation),
                            PK.FromEnum(DimCustomerP.SpanishEducation),
                            PK.FromEnum(DimCustomerP.FrenchEducation),
                            PK.FromEnum(DimCustomerP.EnglishOccupation),
                            PK.FromEnum(DimCustomerP.SpanishOccupation),
                            PK.FromEnum(DimCustomerP.FrenchOccupation),
                            PK.FromEnum(DimCustomerP.HouseOwnerFlag),
                            PK.FromEnum(DimCustomerP.NumberCarsOwned),
                            PK.FromEnum(DimCustomerP.AddressLine1),
                            PK.FromEnum(DimCustomerP.AddressLine2),
                            PK.FromEnum(DimCustomerP.Phone),
                            PK.FromEnum(DimCustomerP.DateFirstPurchase),
                            PK.FromEnum(DimCustomerP.CommuteDistance)
                        }
                    ),
                    (
                        nameof(DimGeography),
                        new List<PK?> {
                            PK.FromEnum(DimGeographyP.City),
                            PK.FromEnum(DimGeographyP.StateOrProvinceCode),
                            PK.FromEnum(DimGeographyP.StateOrProvinceName),
                            PK.FromEnum(DimGeographyP.CountryCode),
                            PK.FromEnum(DimGeographyP.EnglishCountryName),
                            PK.FromEnum(DimGeographyP.SpanishCountryName),
                            PK.FromEnum(DimGeographyP.FrenchCountryName),
                            PK.FromEnum(DimGeographyP.PostalCode),
                            PK.FromEnum(DimGeographyP.DimSalesTerritoryId),
                            PK.FromEnum(DimGeographyP.IpAddressLocator)
                        }
                    ),
                    (
                        nameof(DimProduct),
                        new List<PK?> {
                            PK.FromEnum(DimProductP.AlternateKey),
                            PK.FromEnum(DimProductP.DimProductSubcategoryId),
                            PK.FromEnum(DimProductP.WeightUnitMeasureCode),
                            PK.FromEnum(DimProductP.SizeUnitMeasureCode),
                            PK.FromEnum(DimProductP.EnglishName),
                            PK.FromEnum(DimProductP.SpanishName),
                            PK.FromEnum(DimProductP.FrenchName),
                            PK.FromEnum(DimProductP.StandardCost),
                            PK.FromEnum(DimProductP.FinishedGoodsFlag),
                            PK.FromEnum(DimProductP.Color),
                            PK.FromEnum(DimProductP.SafetyStockLevel),
                            PK.FromEnum(DimProductP.ReorderPoint),
                            PK.FromEnum(DimProductP.ListPrice),
                            PK.FromEnum(DimProductP.Size),
                            PK.FromEnum(DimProductP.SizeRange),
                            PK.FromEnum(DimProductP.Weight),
                            PK.FromEnum(DimProductP.DaysToManufacture),
                            PK.FromEnum(DimProductP.ProductLine),
                            PK.FromEnum(DimProductP.DealerPrice),
                            PK.FromEnum(DimProductP.Class),
                            PK.FromEnum(DimProductP.Style),
                            PK.FromEnum(DimProductP.ModelName),
                            PK.FromEnum(DimProductP.Photo),
                            PK.FromEnum(DimProductP.EnglishDescription),
                            PK.FromEnum(DimProductP.FrenchDescription),
                            PK.FromEnum(DimProductP.ChineseDescription),
                            PK.FromEnum(DimProductP.ArabicDescription),
                            PK.FromEnum(DimProductP.HebrewDescription),
                            PK.FromEnum(DimProductP.ThaiDescription),
                            PK.FromEnum(DimProductP.GermanDescription),
                            PK.FromEnum(DimProductP.JapaneseDescription),
                            PK.FromEnum(DimProductP.TurkishDescription),
                            PK.FromEnum(DimProductP.StartDate),
                            PK.FromEnum(DimProductP.EndDate),
                            PK.FromEnum(DimProductP.Status)
                        }
                    ),
                    (
                        nameof(DimProductCategory),
                        new List<PK?> {
                            PK.FromEnum(DimProductCategoryP.AlternateKey),
                            PK.FromEnum(DimProductCategoryP.EnglishName),
                            PK.FromEnum(DimProductCategoryP.SpanishName),
                            PK.FromEnum(DimProductCategoryP.FrenchName)
                        }
                    ),
                    (
                        nameof(DimProductSubcategory),
                        new List<PK?> {
                            PK.FromEnum(DimProductSubcategoryP.AlternateKey),
                            PK.FromEnum(DimProductSubcategoryP.EnglishName),
                            PK.FromEnum(DimProductSubcategoryP.SpanishName),
                            PK.FromEnum(DimProductSubcategoryP.FrenchName),
                            PK.FromEnum(DimProductSubcategoryP.DimProductCategoryId),
                        }
                    ),
                    (
                        nameof(DimEmployee),
                        new List<PK?> {
                            PK.FromEnum(DimEmployeeP.ParentEmployeeId),
                            PK.FromEnum(DimEmployeeP.EmployeeNationalIDAlternateKey),
                            PK.FromEnum(DimEmployeeP.ParentEmployeeNationalIDAlternateKey),
                            PK.FromEnum(DimEmployeeP.DimSalesTerritoryId),
                            PK.FromEnum(DimEmployeeP.FirstName),
                            PK.FromEnum(DimEmployeeP.LastName),
                            PK.FromEnum(DimEmployeeP.MiddleName),
                            null, // PK.FromEnum(DimEmployeeP.NameStyle),  Was always 0, deleted
                            PK.FromEnum(DimEmployeeP.Title),
                            PK.FromEnum(DimEmployeeP.HireDate),
                            PK.FromEnum(DimEmployeeP.BirthDate),
                            PK.FromEnum(DimEmployeeP.LoginID),
                            PK.FromEnum(DimEmployeeP.EmailAddress),
                            PK.FromEnum(DimEmployeeP.Phone),
                            PK.FromEnum(DimEmployeeP.MaritalStatus),
                            PK.FromEnum(DimEmployeeP.EmergencyContactName),
                            PK.FromEnum(DimEmployeeP.EmergencyContactPhone),
                            PK.FromEnum(DimEmployeeP.SalariedFlag),
                            PK.FromEnum(DimEmployeeP.Gender),
                            PK.FromEnum(DimEmployeeP.PayFrequency),
                            PK.FromEnum(DimEmployeeP.BaseRate),
                            PK.FromEnum(DimEmployeeP.VacationHours),
                            PK.FromEnum(DimEmployeeP.SickLeaveHours),
                            null, // PK.FromEnum(DimEmployeeP.CurrentFlag), Was always 0, deleted
                            PK.FromEnum(DimEmployeeP.SalesPersonFlag),
                            PK.FromEnum(DimEmployeeP.Department),
                            PK.FromEnum(DimEmployeeP.StartDate),
                            PK.FromEnum(DimEmployeeP.EndDate),
                            PK.FromEnum(DimEmployeeP.Status),
                            PK.FromEnum(DimEmployeeP.Photo)
                        }
                    ),
                    (
                        nameof(FactInternetSale),
                        new List<PK?>() {
                            PK.FromEnum(FactInternetSaleP.DimProductId),
                            PK.FromEnum(FactInternetSaleP.OrderDate),
                            PK.FromEnum(FactInternetSaleP.DueDate),
                            PK.FromEnum(FactInternetSaleP.ShipDate),
                            PK.FromEnum(FactInternetSaleP.DimCustomerId),
                            PK.FromEnum(FactInternetSaleP.DimPromotionId),
                            PK.FromEnum(FactInternetSaleP.DimCurrencyId),
                            PK.FromEnum(FactInternetSaleP.DimSalesTerritoryId),
                            PK.FromEnum(FactInternetSaleP.SalesOrderNumber),
                            PK.FromEnum(FactInternetSaleP.SalesOrderLineNumber),
                            null, // PK.FromEnum(FactInternetSaleP.RevisionNumber), Was always 1, deleted
                            null, // PK.FromEnum(FactInternetSaleP.OrderQuantity), Was always 1, deleted
                            null, // PK.FromEnum(FactInternetSaleP.UnitPrice), Was always equal to SalesAmount, deleted
                            null, // PK.FromEnum(FactInternetSaleP.ExtendedAmount), Always equal to SalesAmount, deleted
                            null, // PK.FromEnum(FactInternetSaleP.UnitPriceDiscountPct), Was always 0, deleted
                            null, // PK.FromEnum(FactInternetSaleP.DiscountAmount), Was always 0, deleted
                            null, // PK.FromEnum(FactInternetSaleP.ProductStandardCost), Was always equal to TotalProductCost, deleted
                            PK.FromEnum(FactInternetSaleP.TotalProductCost),
                            PK.FromEnum(FactInternetSaleP.SalesAmount),
                            PK.FromEnum(FactInternetSaleP.TaxAmt),
                            PK.FromEnum(FactInternetSaleP.Freight),
                            PK.FromEnum(FactInternetSaleP.CarrierTrackingNumber),
                            PK.FromEnum(FactInternetSaleP.CustomerPONumber),
                            null, // Ignore, duplicate of OrderDate
                            null, // Ignore, duplicate of DueDate
                            null // Ignore, duplicate of ShipDate
                        }
                    ),
                    (
                        nameof(DimCurrency),
                        new List<PK?> {
                            PK.FromEnum(DimCurrencyP.AlternateKey),
                            PK.FromEnum(DimCurrencyP.Name)
                        }
                    ),
                    (
                        nameof(DimSalesTerritory),
                        new List<PK?>() {
                            null, /// PK.FromEnum(DimSalesTerritoryP.AlternateKey), Not needed / not used. See instead <see cref="DimSalesTerritory.TryGetName"/>
                            PK.FromEnum(DimSalesTerritoryP.Region),
                            PK.FromEnum(DimSalesTerritoryP.Country),
                            PK.FromEnum(DimSalesTerritoryP.Group),
                            PK.FromEnum(DimSalesTerritoryP.Image)
                        }
                    ),
                    (
                        nameof(DimSalesReason),
                        new List<PK?> {
                            PK.FromEnum(DimSalesReasonP.AlternateKey),
                            PK.FromEnum(DimSalesReasonP.Name),
                            PK.FromEnum(DimSalesReasonP.Type)
                        }
                    ),
                    (
                        nameof(FactResellerSale),
                        new List<PK?> {
                            PK.FromEnum(FactResellerSaleP.DimProductId),
                            PK.FromEnum(FactResellerSaleP.OrderDate),
                            PK.FromEnum(FactResellerSaleP.DueDate),
                            PK.FromEnum(FactResellerSaleP.ShipDate),
                            PK.FromEnum(FactResellerSaleP.DimResellerId),
                            PK.FromEnum(FactResellerSaleP.DimEmployeeId),
                            PK.FromEnum(FactResellerSaleP.DimPromotionId),
                            PK.FromEnum(FactResellerSaleP.DimCurrencyId),
                            PK.FromEnum(FactResellerSaleP.DimSalesTerritoryId),
                            PK.FromEnum(FactResellerSaleP.SalesOrderNumber),
                            PK.FromEnum(FactResellerSaleP.SalesOrderLineNumber),
                            PK.FromEnum(FactResellerSaleP.RevisionNumber),
                            PK.FromEnum(FactResellerSaleP.OrderQuantity),
                            PK.FromEnum(FactResellerSaleP.UnitPrice),
                            PK.FromEnum(FactResellerSaleP.ExtendedAmount),
                            PK.FromEnum(FactResellerSaleP.UnitPriceDiscountPct),
                            PK.FromEnum(FactResellerSaleP.DiscountAmount),
                            PK.FromEnum(FactResellerSaleP.ProductStandardCost),
                            PK.FromEnum(FactResellerSaleP.TotalProductCost),
                            PK.FromEnum(FactResellerSaleP.SalesAmount),
                            PK.FromEnum(FactResellerSaleP.TaxAmt),
                            PK.FromEnum(FactResellerSaleP.Freight),
                            PK.FromEnum(FactResellerSaleP.CarrierTrackingNumber),
                            PK.FromEnum(FactResellerSaleP.CustomerPONumber),
                            null, // Ignore, duplicate of OrderDate
                            null, // Ignore, duplicate of DueDate
                            null // Ignore, duplicate of ShipDate
                        }
                    ),
                    (
                        nameof(DimReseller),
                        new List<PK?> {
                            PK.FromEnum(DimResellerP.DimGeographyId),
                            PK.FromEnum(DimResellerP.ResellerAlternateKey),
                            PK.FromEnum(DimResellerP.Phone),
                            PK.FromEnum(DimResellerP.BusinessType),
                            PK.FromEnum(DimResellerP.Name),
                            PK.FromEnum(DimResellerP.NumberEmployees),
                            PK.FromEnum(DimResellerP.OrderFrequency),
                            PK.FromEnum(DimResellerP.OrderMonth),
                            PK.FromEnum(DimResellerP.FirstOrderYear),
                            PK.FromEnum(DimResellerP.LastOrderYear),
                            PK.FromEnum(DimResellerP.ProductLine),
                            PK.FromEnum(DimResellerP.AddressLine1),
                            PK.FromEnum(DimResellerP.AddressLine2),
                            PK.FromEnum(DimResellerP.AnnualSales),
                            PK.FromEnum(DimResellerP.BankName),
                            PK.FromEnum(DimResellerP.MinPaymentType),
                            PK.FromEnum(DimResellerP.MinPaymentAmount),
                            PK.FromEnum(DimResellerP.AnnualRevenue),
                            PK.FromEnum(DimResellerP.YearOpened)
                        }
                    ),
                    (
                        nameof(DimAccount),
                        new List<PK?> {
                            PK.FromEnum(DimAccountP.ParentDimAccountId),
                            PK.FromEnum(DimAccountP.AlternateKey),
                            PK.FromEnum(DimAccountP.ParentAlternateKey),
                            PK.FromEnum(DimAccountP.Description),
                            PK.FromEnum(DimAccountP.Type),
                            PK.FromEnum(DimAccountP.Operator),
                            null, // PK.FromEnum(DimAccountP.CustomMembers), One record 98/9530 with value '[Account].[Accounts].[Account Level 04].&[50]/[Account].[Accounts].[Account Level 02].&[97]'. Else empty. Deleted.
                            PK.FromEnum(DimAccountP.ValueType),
                            PK.FromEnum(DimAccountP.CustomMemberOptions)
                        }
                    ),
                    (
                        nameof(FactFinance),
                        new List<PK?> {
                            PK.FromEnum(FactFinanceP.Date),
                            PK.FromEnum(FactFinanceP.DimOrganizationId),
                            PK.FromEnum(FactFinanceP.DimDepartmentGroupId),
                            PK.FromEnum(FactFinanceP.Scenario),
                            PK.FromEnum(FactFinanceP.DimAccountId),
                            PK.FromEnum(FactFinanceP.Amount),
                            null // Ignore, duplicate of Date
                        }
                    ),
                    (
                        nameof(DimOrganization),
                        new List<PK?> {
                            PK.FromEnum(DimOrganizationP.ParentDimOrganizationId),
                            PK.FromEnum(DimOrganizationP.PercentageOfOwnership),
                            PK.FromEnum(DimOrganizationP.Name),
                            PK.FromEnum(DimOrganizationP.DimCurrencyId)
                        }
                    ),
                    (
                        nameof(DimDepartment),
                        new List<PK?> {
                            PK.FromEnum(DimDepartmentP.ParentDimDepartmentId),
                            PK.FromEnum(DimDepartmentP.Name)
                        }
                    ),
                    (
                        nameof(DimPromotion),
                        new List<PK?> {
                            PK.FromEnum(DimPromotionP.AlternateKey),
                            PK.FromEnum(DimPromotionP.EnglishName),
                            PK.FromEnum(DimPromotionP.SpanishName),
                            PK.FromEnum(DimPromotionP.FrenchName),
                            PK.FromEnum(DimPromotionP.DiscountPct),
                            PK.FromEnum(DimPromotionP.EnglishType),
                            PK.FromEnum(DimPromotionP.SpanishType),
                            PK.FromEnum(DimPromotionP.FrenchType),
                            PK.FromEnum(DimPromotionP.EnglishCategory),
                            PK.FromEnum(DimPromotionP.SpanishCategory),
                            PK.FromEnum(DimPromotionP.FrenchCategory),
                            PK.FromEnum(DimPromotionP.StartDate),
                            PK.FromEnum(DimPromotionP.EndDate),
                            PK.FromEnum(DimPromotionP.MinQty),
                            PK.FromEnum(DimPromotionP.MaxQty)
                        }
                    ),
                    (
                        nameof(FactProductDescription),
                        new List<PK?> {
                            PK.FromEnum(FactProductDescriptionP.DimProductId),
                            PK.FromEnum(FactProductDescriptionP.CultureName),
                            PK.FromEnum(FactProductDescriptionP.Description)
                        }
                    ),
                    (
                        nameof(FactCallCenter),
                        new List<PK?> {
                            PK.FromEnum(FactCallCenterP.Date),
                            PK.FromEnum(FactCallCenterP.WageType),
                            PK.FromEnum(FactCallCenterP.Shift),
                            PK.FromEnum(FactCallCenterP.LevelOneOperators),
                            PK.FromEnum(FactCallCenterP.LevelTwoOperators),
                            PK.FromEnum(FactCallCenterP.TotalOperators),
                            PK.FromEnum(FactCallCenterP.Calls),
                            PK.FromEnum(FactCallCenterP.AutomaticResponses),
                            PK.FromEnum(FactCallCenterP.Orders),
                            PK.FromEnum(FactCallCenterP.IssuesRaised),
                            PK.FromEnum(FactCallCenterP.AverageTimePerIssue),
                            PK.FromEnum(FactCallCenterP.ServiceGrade),
                            null // Ignore, duplicate of Date

                        }
                    ),
                    (
                        nameof(FactCurrencyRate),
                        new List<PK?> {
                            PK.FromEnum(FactCurrencyRateP.DimCurrencyId),
                            PK.FromEnum(FactCurrencyRateP.Date),
                            PK.FromEnum(FactCurrencyRateP.AverageRate),
                            PK.FromEnum(FactCurrencyRateP.EndOfDayRate),
                            null // Ignore, duplicate of Date
                        }
                    ),
                    (
                        nameof(FactInternetSaleReason),
                        new List<PK?> {
                            PK.FromEnum(FactInternetSaleReasonP.SalesOrderNumber),
                            PK.FromEnum(FactInternetSaleReasonP.SalesOrderLineNumber),
                            PK.FromEnum(FactInternetSaleReasonP.DimSalesReasonId)
                        }
                    ),
                    (
                        nameof(FactSalesQuota),
                        new List<PK?> {
                            PK.FromEnum(FactSalesQuotaP.DimEmployeeId),
                            PK.FromEnum(FactSalesQuotaP.Date),
                            PK.FromEnum(FactSalesQuotaP.CalendarYear),
                            PK.FromEnum(FactSalesQuotaP.CalendarQuarter),
                            PK.FromEnum(FactSalesQuotaP.SalesAmountQuota),
                            null // Ignore, duplicate of Date
                        }
                    ),
                    (
                        nameof(FactSurveyResponse),
                        new List<PK?> {
                            PK.FromEnum(FactSurveyResponseP.Date),
                            PK.FromEnum(FactSurveyResponseP.DimCustomerId),
                            PK.FromEnum(FactSurveyResponseP.DimProductCategoryId),
                            PK.FromEnum(FactSurveyResponseP.EnglishProductCategoryName),
                            PK.FromEnum(FactSurveyResponseP.DimProductSubcategoryId),
                            PK.FromEnum(FactSurveyResponseP.EnglishProductSubcategoryName),
                            null // Ignore, duplicate of Date
                        }
                    )
                };

                var toFolder = Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + "Data";

                static byte[] ToHexByte(string inputHex) {
                    // Borrowed from
                    // https://stackoverflow.com/questions/46327156/convert-a-hex-string-to-base64
                    var resultantArray = new byte[inputHex.Length / 2];
                    for (var i = 0; i < resultantArray.Length; i++) {
                        resultantArray[i] = System.Convert.ToByte(inputHex.Substring(i * 2, 2), 16);
                    }
                    return resultantArray;
                }

                var usCulture = new System.Globalization.CultureInfo("en-US", useUserOverride: false); // Probably unnecessary to specify.
                var validDateTimeFormats = new string[] {
                    "yyyy-MM-dd",
                    "yyyy-MM-dd HH:mm:ss",
                    "yyyyMMdd",
                };

                files.ForEach(file => {
                    var filename =
                        file.entityType switch {
                            nameof(FactInternetSale) => "FactInternetSales",                                    // Use the singular
                            nameof(FactInternetSaleReason) => "FactInternetSalesReason",                        // Use the singular
                            nameof(FactResellerSale) => "FactResellerSales",                                    // Use the singular
                            nameof(FactProductDescription) => "FactAdditionalInternationalProductDescription",  // Simplify name, too long originally
                            nameof(DimDepartment) => "DimDepartmentGroup",                                      // Simplify name
                            _ => file.entityType
                        } +
                        ".csv";
                    var sourcePath = sourceFolder + filename;
                    logger.LogKeyValue(nameof(sourcePath), sourcePath);
                    if (!System.IO.File.Exists(sourcePath)) {
                        logger.LogText("Not found");
                        throw new AdventureWorksConversionException(
                            nameof(sourcePath) + " " + sourcePath + " not found.\r\n" +
                            "\r\n" +
                            resolution
                        );
                    }
                    logger.LogText("ReadAllLines");
                    var ps = new List<string>();
                    System.IO.File.ReadAllLines(sourcePath).ForEach(l => {
                        var fields = l.Split("|");

                        switch (file.entityType) {
                            // Add primary key at start for those files that does not have the primary key as first item
                            case nameof(FactInternetSale): {
                                    var temp = fields.ToList();
                                    temp.Insert(0, fields[8] + "_" + fields[9]); /// <see cref="FactInternetSaleP.SalesOrderNumber"/> and <see cref="FactInternetSaleP.SalesOrderLineNumber"/>
                                    fields = temp.ToArray();
                                    break;
                                }
                            case nameof(FactResellerSale): {
                                    var temp = fields.ToList();
                                    temp.Insert(0, fields[9] + "_" + fields[10]); /// <see cref="FactResellerSaleP.SalesOrderNumber"/> and <see cref="FactResellerSaleP.SalesOrderLineNumber"/>
                                    fields = temp.ToArray();
                                    break;
                                }
                            case nameof(FactCurrencyRate): {
                                    var temp = fields.ToList();
                                    temp.Insert(0, fields[0] + "_" + fields[1]); /// <see cref="FactCurrencyRateP.DimCurrencyId"/> and <see cref="FactCurrencyRateP.Date"/>
                                    fields = temp.ToArray();
                                    break;
                                }
                            case nameof(FactInternetSaleReason): {
                                    var temp = fields.ToList();
                                    temp.Insert(0, fields[0] + "_" + fields[1] + fields[2]); /// <see cref="FactInternetSaleReasonP.SalesOrderNumber"/>, <see cref="FactInternetSaleReasonP.SalesOrderLineNumber"/> and <see cref="FactInternetSaleReasonP.DimSalesReasonId"/>
                                    fields = temp.ToArray();
                                    break;
                                }
                            case nameof(FactProductInventory): {
                                    var temp = fields.ToList();
                                    temp.Insert(0, fields[0] + "_" + fields[1]); /// <see cref="FactProductInventoryP.DimProductId"/> and <see cref="FactProductInventoryP.Date"/>
                                    fields = temp.ToArray();
                                    break;
                                }
                            case nameof(FactProductDescription): {
                                    var temp = fields.ToList();
                                    temp.Insert(0, fields[0] + "_" + fields[1]); /// <see cref="FactProductDescriptionP.DimProductId"/> and <see cref="FactProductDescriptionP.CultureName"/>
                                    fields = temp.ToArray();
                                    break;
                                }
                            case nameof(DimSalesTerritory):
                                if ("11|0|NA|NA|NA|".Equals(l)) {
                                    // The N/A territory, value "11" gets removed below anyway.
                                    return;
                                }
                                break;
                        }


                        // First item in fields is the id-field (primary key)
                        if (fields.Length != (file.fields.Count + 1)) throw new AdventureWorksConversionException(
                            "Invalid line '" + l + "' in " + sourcePath + ", not " + (file.fields.Count + 1) + " elements but " + fields.Length
                        );

                        for (var i = 0; i < file.fields.Count; i++) {
                            var pk = file.fields[i];
                            if (pk == null) continue;

                            var value = fields[i + 1]; // +1 because key is in t[0], and that does not count as a field.
                            if ("".Equals(value)) {
                                // No need in AgoRapide to specify null
                                continue;
                            }

                            var strFieldName = pk.ToString();
                            if (strFieldName.EndsWith("Date")) {
                                // A lot of the dates have an unnecessary time-component in them like '2010-12-29 00:00:00'.
                                // Other dates have a non-standard format like '20111029' (linked to DimDate)
                                // Change to standard AgoRapide format.
                                if (!DateTime.TryParseExact(value, validDateTimeFormats, usCulture, System.Globalization.DateTimeStyles.None, out var dtmValue)) {
                                    throw new AdventureWorksConversionException("DateTime.TryParseExact for field " + file.fields[i] + " (" + value + ") in\r\n" + l);
                                }
                                value = dtmValue.ToString("yyyy-MM-dd");
                            } else {
                                switch (strFieldName) {
                                    case nameof(DimEmployeeP.DimSalesTerritoryId): /// Does also include <see cref="DimGeographyP.DimSalesTerritoryId"/>, <see cref="FactInternetSaleP.DimSalesTerritoryId"/>, <see cref="FactResellerSaleP.DimSalesTerritoryId"/> but for these value "11" is not used anyway
                                        if ("11".Equals(value)) continue; // Ignore, the N/A territory (better with null value)
                                        break;
                                    case nameof(DimEmployeeP.SalariedFlag):
                                    case nameof(DimEmployeeP.SalesPersonFlag):
                                        if ("0".Equals(value)) continue; // Ignore
                                        break;
                                    case nameof(FactInternetSaleP.DimPromotionId): /// Does also include <see cref="FactResellerSaleP.DimPromotionId"/>
                                        if ("1".Equals(value)) continue; // Ignore, "No discount"
                                        break;
                                    case nameof(FactResellerSaleP.DiscountAmount):
                                    case nameof(FactResellerSaleP.UnitPriceDiscountPct):
                                        if ("0".Equals(value)) continue; // Ignore
                                        break;
                                    case nameof(FactFinanceP.Scenario):
                                        // Replaces a whole table with an enum.
                                        value = value switch {
                                            "1" => nameof(Scenario.Actual),
                                            "2" => nameof(Scenario.Budget),
                                            "3" => nameof(Scenario.Forecast),
                                            _ => throw new AdventureWorksConversionException("Illegal scenario in " + file.fields[i] + " (" + value + ") in\r\n" + l)
                                        };
                                        break;
                                    case nameof(DimSalesTerritoryP.Image):
                                    case nameof(DimEmployeeP.Photo): /// Does also include <see cref="DimProductP.Photo"/>
                                                                     /// Convert to base 64 now. 
                                                                     /// This saves signifiant space in property stream and makes for less work in <see cref="PictureEncoder.Encode"/>
                                        value = System.Convert.ToBase64String(ToHexByte(value)); // [(78 * 2)..]));
                                        if (
                                            value.StartsWith("R0lGODlh8ACVAPcAAAAAAIAAAACAAICAAAAAgIAAgACAg") &&
                                            value.EndsWith("bjnrvvuvPfu++/ABy/88MQXb/zxyCev/PLMCxQQADs=")) {
                                            // This is the image with the text "No Image Available" for DimProductP.LargePhoto. No need for storing.
                                            continue;
                                        }
                                        break;
                                }
                            }

                            if (pk.Type == typeof(double) && value.Contains("E")) {
                                // Looks like scientific notation which is not support by AgoRapide as standard
                                if (!double.TryParse(value, System.Globalization.NumberStyles.Float, null, out var temp)) {
                                    throw new AdventureWorksConversionException("!double.TryParse for field " + file.fields[i] + " in\r\n" + l);
                                }
                                value = temp.ToString("0.00");
                            }

                            if (!pk.TryValidateAndParse(value, out var result)) throw new AdventureWorksConversionException("!pk.TryValidateAndParse (" + result.ErrorResponse + ") for field " + file.fields[i] + " in\r\n" + l);

                            if (pk.Type == typeof(double)) {
                                value = ((double)result.Result!).ToString("0.00"); // Use two decimals for all doubles (removes spurious decimals for instance)
                            }

                            ps.Add(nameof(PSPrefix.dt) + "/" +
                                file.entityType + "/" +
                                fields[0] + "/" + // Entity primary key
                                strFieldName + " = " +

                                // NOTE: You can also use a one-liner here:
                                // NOTE: value.Replace("0x", "0xoooo").Replace("\r", "0x000D").Replace("\n", "0x000A").Replace(";", "0x003B");
                                PropertyStreamLine.EncodeValuePart(value) // Value
                            );
                        }
                    });

                    logger.LogKeyValue(nameof(ps) + ".Count", ps.Count.ToString());

                    // Leading _ should ensure that file will be read first at next startup
                    var psPath = toFolder + slash + "_" + file.entityType + ".txt";
                    logger.LogKeyValue(nameof(psPath), psPath);
                    System.IO.File.WriteAllText(psPath, string.Join("\r\n", ps) + "\r\n"); // Extremely important, remember trailing line break

                });

                throw new AdventureWorksSuccessfulException(
                    "Files in " + sourceFolder + " has been successfully converted to " + nameof(ARConcepts.PropertyStream) + "-format in " + toFolder + ".\r\n" +
                    "Resolution:\r\nRestart application in order to continue."
                );
            }
        }

        private class AdventureWorksConversionException : ApplicationException {
            public AdventureWorksConversionException(string message) : base(message) { }
            public AdventureWorksConversionException(string message, Exception innerException) : base(message, innerException) { }
        }

        private class AdventureWorksSuccessfulException : ApplicationException {
            public AdventureWorksSuccessfulException(string message) : base(message) { }
            public AdventureWorksSuccessfulException(string message, Exception innerException) : base(message, innerException) { }
        }
    }
}
