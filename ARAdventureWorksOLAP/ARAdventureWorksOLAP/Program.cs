// Copyright (c) 2021 Bj�rn Erling Fl�tten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ARCCore;
using ARCDoc;
using ARCAPI;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Application startup and initialization")]
    public class Program {

        public static IK NodeId = IKString.FromString(System.Environment.MachineName);
        public static DataStorage DataStorage { get; private set; } = null!; // Initialized here only in order to get rid of compilation warning. 

        public static void Main(string[] args) {
            System.Threading.Thread.CurrentThread.Name = "MainThread";

            /// Include all assemblies in which your controllers and <see cref="AgoRapide.BaseEntity"/>-derived classes resides.
            UtilCore.Assemblies = new List<System.Reflection.Assembly> {
                typeof(ARCCore.ClassAttribute).Assembly,
                typeof(ARCDoc.Documentator).Assembly,
                typeof(ARCQuery.QueryExpression).Assembly,
                typeof(ARCAPI.BaseController).Assembly,
                typeof(ARAdventureWorksOLAP.Program).Assembly
            };

            // 'Inform' ARCQuery about application specific function keys:
            ARCQuery.FunctionKey.AddParser("AWFISCALYEAR", value => FunctionKeyAWFiscalYear.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            using var streamProcessor = StreamProcessor.CreateBareBonesInstance(NodeId);
            DataStorage = DataStorage.Create(NodeId, storage: new Root(), streamProcessor);

            /// We have a dilemma regarding the rest of the initialization. 
            /// Should we do it 1) All now (blocking the startup thread), or 2) In a separate thread
            /// Alternative 1) gives an unresponsive API at startup (possible regarded as an error condition by IIS / ASP.NET Core)
            /// and long compile, run cycles for the developer.
            /// Alternative 2) means that queries return incomplete data at startup
            /// We choose alternative 2), but put a warning about incomplete data 
            /// (<see cref="BaseController.GetEventualTimestampIsOldWarningMessageHTML"/> which uses <see cref="StreamProcessorP.TimestampIsOld"/>)
            Task.Factory.StartNew(() => {
                try {
                    System.Threading.Thread.CurrentThread.Name = "Initialization";
                    System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started

                    /// This value will be sent by the Stream processor at call to <see cref="StreamProcessor.Initialize"/>, 
                    /// assumed to happen "at once", but not formally proven to happen before that API is ready to handle requests. 
                    /// To be on the formal safe side we therefore set it ourselves now
                    DataStorage.Storage.SetPV(StreamProcessorP.TimestampIsOld, true);

                    InitializeStreamProcessorAndBuildDataStorage(streamProcessor);
                } catch (Exception ex) {
                    try {
                        DataStorage.Lock.EnterWriteLock();
                        PropertyStreamLine.StoreFailure(DataStorage.Storage, "[InitializationFailed]", UtilCore.GetExeptionDetails(ex), nameof(PropertyStreamLine.TryParseAndStore));
                    } catch (Exception) {
                        // Give up totally
                    } finally {
                        DataStorage.Lock.ExitWriteLock();
                    }
                }
            });
            AddController.AllowFilter = new List<Subscription> {
                Subscription.Parse("-*") // June 2021: No edits allowed. Experience showed that damaging edits were done which messed up the basic structure.
            };
            Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>()).Build().Run();
        }

        [ClassMember(Description =
            "Initializes stream processor.\r\n" +
            "Must be called after -" + nameof(DataStorage) + "- has been initialized.\r\n" +
            "\r\n" +
            "Note that uses parallell processing for parsing to -" + nameof(PropertyStreamLineParsed) + "-.\r\n"
        )]
        public static void InitializeStreamProcessorAndBuildDataStorage(StreamProcessor streamProcessor) {

            var CAPACITY = 1000;
            var bulkList = new List<string>(CAPACITY); // Parse in bulk, because parsing can be done in parallell (saves some seconds)
            var emptyBulkList = new Action(() => {
                // Step 1, Parse                
                var bulkListParsed = bulkList.
                    AsParallel().
                    AsOrdered(). // AsOrdered is EXTREMELY IMPORTANT, because later storing must be done in order of parse result.
                    Select(s => { // Step 1, parse all items (can be done in parallell)
                        if (!PropertyStreamLineParsed.TryParse(s, out var retval, out var errorResponse)) {
                            try {
                                // Remember inside AsParallell now, use locking!
                                // (actually we must lock anyway after 17 Jul 2020 because initialization runs on a separate thread)
                                DataStorage.Lock.EnterWriteLock();
                                PropertyStreamLine.StoreFailure(DataStorage.Storage, s, errorResponse, nameof(PropertyStreamLineParsed.TryParse));
                            } finally {
                                DataStorage.Lock.ExitWriteLock();
                            }
                            return null;
                        }
                        return retval;
                    }).ToList(); // EXTREMELY IMPORTANT, because of locking, avoid deferred execution (force execution now by converting to List)
                // Step 2, Store
                try {
                    // After 17 Jul 2020 we must lock here because initialization now runs on a separate thread)
                    DataStorage.Lock.EnterWriteLock();
                    bulkListParsed.ForEach(s => { // Step 2, store must be done sequentially, therefore AsOrdered above
                        if (s == null) return; // Parsing failed above
                        if (!PropertyStreamLine.TryStore(DataStorage.Storage, s, out var errorResponse)) {
                            PropertyStreamLine.StoreFailure(DataStorage.Storage, s.ToString(), errorResponse, nameof(PropertyStreamLine.TryStore));
                        }
                    });
                } finally {
                    DataStorage.Lock.ExitWriteLock();
                }
                bulkList = new List<string>(CAPACITY);
            });

            /// NOTE: In order to see effectiveness of parallell parsing, use this alternative instead of code below
            /// NOTE: streamProcessor.OutsideLocalReceiver = <see cref="ParseAndStoreFailSafePropertyStreamLine"/>
            
            // NOTE: Parallell parsing gives roughly a 50% performance increase (that is, a one-third reduction of the startup time)
            // NOTE: This as measured Feb 2021 on a ThinkPad T470p laptop, i7-7700hq, 4 real cores, 8 threads.
            streamProcessor.OutsideLocalReceiver = new Action<string>(propertyStreamLine => {
                var s = ModifyPropertyStreamLine(propertyStreamLine); // Filter out those not wanted
                if (s == null) return;
                bulkList.Add(s);
                if (bulkList.Count >= CAPACITY) {
                    emptyBulkList();
                }
            });

            // This call may take some time. Data storage will be built now from file
            streamProcessor.Initialize();
            emptyBulkList(); // Empty remaining items.

            // This method is only relevant for developers of ARAdventureWorksOLAP, if content of Data-folder is to be updated
            // (delete all content of Data-foler in order for this method to have any function)
            Converter.ConvertDatabase(streamProcessor);

            streamProcessor.OutsideLocalReceiver = ParseAndStoreFailSafePropertyStreamLine;
        }

        private static string _dt = nameof(PSPrefix.dt) + "/";
        private static string _timestamp = nameof(StreamProcessorP.Timestamp) + " = ";
        private static string _timestampIsOld = nameof(StreamProcessorP.TimestampIsOld) + " = ";
        private static int _daysToAdd = (int)DateTime.UtcNow.Subtract(new DateTime(2014, 1, 28)).TotalDays; // Last FactInternetSale.OrdreDate in database

        [ClassMember(Description = "Return value of NULL means that shall be ignored (not dt/, not Timestamp =)")]
        private static string? ModifyPropertyStreamLine(string propertyStreamLine) {
            if (!propertyStreamLine.StartsWith(_dt)) {
                if (
                    propertyStreamLine.StartsWith(_timestamp) || /// NOTE: As of Mar 2021 ARAdventureWorksOLAP does not use <see cref="StreamProcessorP.Timestamp"/> (since original datadump is not ordered chronologically)
                    propertyStreamLine.StartsWith(_timestampIsOld)
                ) {  // Timestamp are stored at root-level, they do not start with dt/ in property stream
                    return propertyStreamLine;
                }
                return null;
            } else {
                var pos = propertyStreamLine.IndexOf("Date = ");
                if (pos > 1) {
                    pos += "Date = ".Length;
                    if (pos + 10 <= propertyStreamLine.Length && UtilCore.DateTimeTryParse(propertyStreamLine.Substring(pos, 10), out var dateTime) && dateTime.Year < 2021) {
                        /// This looks like an old date
                        /// Bring dates "up-to-date", in order for the database to look fresh
                        /// Note that any new date values added after conversion may look strange now, as they will not be changed.
                        /// (we do not know WHEN they where changed with the current setup (not using <see cref="PP.Created"/>
                        dateTime = dateTime.AddDays(_daysToAdd);
                        propertyStreamLine = propertyStreamLine.Substring(0, pos) + dateTime.ToString("yyyy-MM-dd") + propertyStreamLine[(pos + 10)..];
                    }
                }
                return propertyStreamLine;
            }
        }

        [ClassMember(Description =
            "Note less strict handling when application is up and running.\r\n" +
            "The last failure can at any time be found at root-level in the data storage " +
            "with the key 'ParseOrStoreFailure' (see -" + nameof(PropertyStreamLine.StoreFailure) + "-)."
        )]
        private static void ParseAndStoreFailSafePropertyStreamLine(string propertyStreamLine) {
            var s = ModifyPropertyStreamLine(propertyStreamLine);
            if (s == null) return;
            try {
                DataStorage.Lock.EnterWriteLock();
                PropertyStreamLine.ParseAndStoreFailSafe(DataStorage.Storage, s);
            } finally {
                DataStorage.Lock.ExitWriteLock();
            }
        }
    }
}