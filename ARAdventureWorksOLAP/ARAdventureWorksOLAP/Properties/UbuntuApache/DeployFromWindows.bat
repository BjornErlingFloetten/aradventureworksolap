REM Ensure that Build | Publish ARAdventureWorksOLAP was done in Visual Studio.
REM
REM Ensure that station Z: is mapped to root of the web server
REM
REM In other words, ensure that the following folders are available:
REM Z:\etc\apache2\sites-enabled (for copying of file ARAdventureWorksOLAP.conf)
REM Z:\etc\systemd\system (for copying of file ARAdventureWorksOLAP.service)
REM Z:\var\www\app\ARAdventureWorksOLAP (for copying of ARAdventureWorksOLAP\bin\Release\net5.0\publish)

PAUSE

COPY ARAdventureWorksOLAP.conf Z:\etc\apache2\sites-enabled
COPY ARAdventureWorksOLAP.service Z:\etc\systemd\system
COPY ..\..\bin\Release\net5.0\publish\*.* Z:\var\www\app\ARAdventureWorksOLAP

REM Most relevant commands to execute on server now are:
REM   systemctl restart ARAdventureWorksOLAP.service

REM If ARAdventureWorksOLAP.conf was changed:
REM   apache2ctl configtest
REM   systemctl restart apache2

REM If ARAdventureWorksOLAP.service was changed:
REM   systemctl daemon-reload
REM   systemctl restart ARAdventureWorksOLAP.service

REM If this was initial publish, additional steps must also be taken.
REM See ARAdventureWorksOLAP.conf and ARAdventureWorksOLAP.service for details.
REM All _Fact and _Dim files in Data-folder must be copied to \var\www\app\ARAdventureWorksOLAP\Data
REM Also, ensure that folder \var\www\app\ARAdventureWorksOLAP\Data AND files within have necessary write rights.

PAUSE


