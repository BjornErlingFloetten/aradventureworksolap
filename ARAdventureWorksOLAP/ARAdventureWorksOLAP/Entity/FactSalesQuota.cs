﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a Sales quota. See also -" + nameof(FactSalesQuotaP) + "- and -" + nameof(FactSalesQuotaCollection) + "-.")]
    public class FactSalesQuota : PExact<FactSalesQuotaP> {
        public FactSalesQuota() : base(capacity: 5) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
    }
    
    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(FactSalesQuotaP.DimEmployeeId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactSalesQuota) + "- instances.\r\n"
    )]
    public class FactSalesQuotaCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(FactSalesQuota) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum FactSalesQuotaP {
        __invalid,
        [PKType(Description = "Called 'EmployeeKey' in original sample database")]
        DimEmployeeId,
        [PKType(Description = "Called 'DateKey' in original sample database")]
        Date,
        CalendarYear,
        CalendarQuarter,
        SalesAmountQuota,
        // Unnecessary duplicate.
        // Date
    }
}
