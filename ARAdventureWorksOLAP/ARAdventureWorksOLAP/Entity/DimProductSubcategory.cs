﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a ProductSubcategory. See also -" + nameof(DimProductSubcategoryP) + "- and -" + nameof(DimProductSubcategoryCollection) + "-.")]
    public class DimProductSubcategory : PExact<DimProductSubcategoryP> {
        public DimProductSubcategory() : base(capacity: 5) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Returns " + nameof(DimProductSubcategoryP.EnglishName) + ".\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetName(out IP retval, out string errorResponse) => IP.TryGetP(DimProductSubcategoryP.EnglishName, out retval, out errorResponse);

        [ClassMember(Description =
            "Returns -" + nameof(DimProductCategoryP.EnglishName) + "- through -" + nameof(DimProductSubcategoryP.DimProductCategoryId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCategory(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimProductSubcategoryP.DimProductCategoryId), foreignField: PK.FromEnum(DimProductCategoryP.EnglishName), out retval, out errorResponse);

    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(DimProductSubcategoryP.DimProductCategoryId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(DimCustomer) + "- instances.\r\n"
    )]
    public class DimProductSubcategoryCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(DimProductSubcategory) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimProductSubcategoryP {
        __invalid,
        [PKType(Description = "Called 'ProductSubcategoryAlternateKey' in original sample database")]
        AlternateKey,
        [PKType(Description = "Called 'EnglishProductSubcategoryName' in original sample database")]
        EnglishName,
        [PKType(Description = "Called 'SpanishProductSubcategoryName' in original sample database")]
        SpanishName,
        [PKType(Description = "Called 'FrenchProductSubcategoryName' in original sample database")]
        FrenchName,
        [PKType(Description = "Called 'ProductCategoryKey' in original sample database")]
        DimProductCategoryId
    }
}