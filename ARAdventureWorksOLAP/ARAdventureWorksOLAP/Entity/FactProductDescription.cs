﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = 
        "Represents a Product description. See also -" + nameof(FactProductDescriptionP) + "- and -" + nameof(FactProductDescriptionCollection) + "-.\r\n" +
        "\r\n" +
        "Called 'FactAdditionalInternationProductDescription' in original sample database"
    )]
    public class FactProductDescription : PExact<FactProductDescriptionP> {
        public FactProductDescription() : base(capacity: 3) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(FactProductDescriptionP.DimProductId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactProductDescription) + "- instances.\r\n"
    )]
    public class FactProductDescriptionCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(FactProductDescription) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum FactProductDescriptionP {
        __invalid,
        [PKType(Description = "Called 'ProductKey' in original sample database")]
        DimProductId,
        CultureName,
        [PKType(Description = "Called 'ProductDescription' in original sample database")]
        Description,
    }
}
