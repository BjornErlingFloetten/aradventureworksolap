﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

	[Class(Description = "Represents a SalesReason. See also -" + nameof(DimSalesReasonP) + "-.")]
	public class DimSalesReason : PExact<DimSalesReasonP> {
		public DimSalesReason() : base(capacity: 3) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

	}

	// Unnecessary, no foreign keys in this collection / table
	//public class DimSalesReasonCollection : PCollection {
	//}

	[Enum(
		Description = "Describes class -" + nameof(DimSalesReason) + "-.",
		AREnumType = AREnumType.PropertyKeyEnum
	)]
	public enum DimSalesReasonP {
		__invalid,
		[PKType(Description = "Called 'SalesReasonAlternateKey' in original sample database")]
		AlternateKey,
		[PKType(Description = "Called 'SalesReasonName' in original sample database")]
		Name,
		[PKType(Description = "Called 'SalesReasonReasonType' in original sample database")]
		Type,
	}
}
