﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

	[Class(Description = 
		"Represents an Internet sale reason. See also -" + nameof(FactInternetSaleReasonP) + "- and -" + nameof(FactInternetSaleReasonCollection) + "-.\r\n" +
		"\r\n" +
		"Called 'FactInternetSalesReason ' in original sample database"
	)]
	public class FactInternetSaleReason : PExact<FactInternetSaleReasonP> {
		public FactInternetSaleReason() : base(capacity: 3) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
	}

	[Class(Description =
		"The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(FactInternetSaleReasonP.DimSalesReasonId) + "-.\r\n" +
		"\r\n" +
		"-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactInternetSaleReason) + "- instances.\r\n"
	)]
	public class FactInternetSaleReasonCollection : PCollection {
	}

	[Enum(
		Description = "Describes class -" + nameof(FactInternetSaleReason) + "-.",
		AREnumType = AREnumType.PropertyKeyEnum
	)]
	public enum FactInternetSaleReasonP {
		__invalid,
		SalesOrderNumber,
		SalesOrderLineNumber,
		[PKType(Description = "Called 'SalesReasonKey' in original sample database")]
		DimSalesReasonId
	}
}
