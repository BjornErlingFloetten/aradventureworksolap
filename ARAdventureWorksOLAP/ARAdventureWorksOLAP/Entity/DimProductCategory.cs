﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a ProductCategory. See also -" + nameof(DimProductCategoryP) + "-.")]
    public class DimProductCategory : PExact<DimProductCategoryP> {
        public DimProductCategory() : base(capacity: 4) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Returns " + nameof(DimProductCategoryP.EnglishName) + ".\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetName(out IP retval, out string errorResponse) => IP.TryGetP(DimProductCategoryP.EnglishName, out retval, out errorResponse);

    }

    // Unnecessary, no foreign keys in this collection / table
    //public class DimProductCategoryCollection : PCollection {
    //}

    [Enum(
        Description = "Describes class -" + nameof(DimProductCategory) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimProductCategoryP {
        __invalid,
        [PKType(Description = "Called 'ProductCategoryAlternateKey' in original sample database")]
        AlternateKey,
        [PKType(Description = "Called 'EnglishProductCategoryName' in original sample database")]
        EnglishName,
        [PKType(Description = "Called 'SpanishProductCategoryName' in original sample database")]
        SpanishName,
        [PKType(Description = "Called 'FrenchProductCategoryName' in original sample database")]
        FrenchName
    }
}