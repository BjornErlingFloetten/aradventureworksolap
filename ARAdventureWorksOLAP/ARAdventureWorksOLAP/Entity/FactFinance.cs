﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a Financial transaction. See also -" + nameof(FactFinanceP) + "- and -" + nameof(FactFinanceCollection) + "-.")]
    public class FactFinance : PExact<FactFinanceP> {
        public FactFinance() : base(capacity: 6) { } // There is a slight performance improvement at application initialization by stating capacity explicit now        
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign keys like " +
        "-" + nameof(FactFinanceP.DimOrganizationId) + "-, " +
        "-" + nameof(FactFinanceP.DimDepartmentGroupId) + "- and " +
        "-" + nameof(FactFinanceP.DimAccountId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactFinance) + "- instances.\r\n"
    )]
    public class FactFinanceCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(FactFinance) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum FactFinanceP {
        __invalid,
        [PKType(Type = typeof(DateTime), Description = "Called 'DateKey' in original sample database")]
        Date,
        [PKType(Description = "Called 'OrganizationKey' in original sample database")]
        DimOrganizationId,
        [PKType(Description = "Called 'DepartmentGroupKey' in original sample database")]
        DimDepartmentGroupId,
        [PKType(Type = typeof(Scenario), Description = "Called 'ScenarioKey' in original sample database. Original foreign key (to DimScenario-table). Replaced with enum here")]
        Scenario,
        [PKType(Description = "Called 'AccountKey' in original sample database")]
        DimAccountId,
        [PKType(Type = typeof(double))]
        Amount,
        // Unnecessary duplicate
        // Date
    }

    [Enum(
        Description = "Replaces table DimScenario in original sample database",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum Scenario {
        __invalid,
        Actual,
        Budget,
        Forecast
    }
}
