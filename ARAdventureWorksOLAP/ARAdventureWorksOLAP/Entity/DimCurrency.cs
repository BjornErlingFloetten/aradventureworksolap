﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

	[Class(Description = "Represents a Currency. See also -" + nameof(DimCurrencyP) + "-.")]
	public class DimCurrency : PExact<DimCurrencyP> {
		public DimCurrency() : base(capacity: 2) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

	}

	// Unnecessary, no foreign keys in this collection / table
	//public class DimCurrencyCollection : PCollection {
	//}

	[Enum(
		Description = "Describes class -" + nameof(DimCurrency) + "-.",
		AREnumType = AREnumType.PropertyKeyEnum
   )]
	public enum DimCurrencyP {
		__invalid,
		[PKType(Description = "The ISO-4217 3-letter abbreviation like NOK, EUR, USD and so on. Called 'CurrencyAlternateKey' in original sample database")]
		AlternateKey,
		[PKType(Description = "Called 'CurrencyName' in original sample database")]
		Name,
	}
}
