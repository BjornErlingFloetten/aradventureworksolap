﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

	[Class(Description = "Represents a Geography location. See also -" + nameof(DimGeographyP) + "-.")]
	public class DimGeography : PExact<DimGeographyP> {
		public DimGeography() : base(capacity: 10) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

		[ClassMember(Description =
			"Returns " + nameof(DimGeographyP.EnglishCountryName) + ".\r\n" +
			"\r\n" +
			"Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
		)]
		public bool TryGetCountry(out IP retval, out string errorResponse) => IP.TryGetP(DimGeographyP.EnglishCountryName, out retval, out errorResponse);

		[ClassMember(Description =
			"Returns " + nameof(DimGeographyP.StateOrProvinceName) + ".\r\n" +
			"\r\n" +
			"Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
		)]
		public bool TryGetState(out IP retval, out string errorResponse) => IP.TryGetP(DimGeographyP.StateOrProvinceName, out retval, out errorResponse);

		[ClassMember(Description =
			"Returns " + nameof(DimGeographyP.StateOrProvinceCode) + ".\r\n" +
			"\r\n" +
			"Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
		)]
		public bool TryGetStateCode(out IP retval, out string errorResponse) => IP.TryGetP(DimGeographyP.StateOrProvinceCode, out retval, out errorResponse);

		[ClassMember(Description =
			"Returns " + nameof(DimGeographyP.StateOrProvinceName) + ".\r\n" +
			"\r\n" +
			"Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
		)]
		public bool TryGetProvince(out IP retval, out string errorResponse) => IP.TryGetP(DimGeographyP.StateOrProvinceName, out retval, out errorResponse);

		[ClassMember(Description =
			"Returns " + nameof(DimGeographyP.StateOrProvinceCode) + ".\r\n" +
			"\r\n" +
			"Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
		)]
		public bool TryGetProvinceCode(out IP retval, out string errorResponse) => IP.TryGetP(DimGeographyP.StateOrProvinceCode, out retval, out errorResponse);
	}

	// Unnecessary, no foreign keys in this collection / table
	//public class DimGeographyCollection : PCollection {
	//}

	[Enum(
		Description = "Describes class -" + nameof(DimGeography) + "-.",
		AREnumType = AREnumType.PropertyKeyEnum
	)]
	public enum DimGeographyP {
		__invalid,
		City,
		[PKType(Description = "Called 'StateProvinceCode' in original sample database")]
		StateOrProvinceCode,
		[PKType(Description = "Called 'StateProvinceName' in original sample database")]
		StateOrProvinceName,
		[PKType(Description = "Called 'CountryRegionCode' in original sample database")]
		CountryCode,
		[PKType(Description = "Called 'EnglishCountryRegionName' in original sample database")]
		EnglishCountryName,
		[PKType(Description = "Called 'SpanishCountryRegionName' in original sample database")]
		SpanishCountryName,
		[PKType(Description = "Called 'FrenchCountryRegionName' in original sample database")]
		FrenchCountryName,
		PostalCode,
		[PKType(Description = "Called 'SalesTerritoryKey' in original sample database")]
		DimSalesTerritoryId,
		IpAddressLocator
	}
}
