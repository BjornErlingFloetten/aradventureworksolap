﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a CurrencyRate. See also -" + nameof(FactCurrencyRateP) + "- and -" + nameof(FactCurrencyRateCollection) + "-.")]
    public class FactCurrencyRate : PExact<FactCurrencyRateP> {
        public FactCurrencyRate() : base(capacity: 4) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(FactCurrencyRateP.DimCurrencyId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactResellerSale) + "- instances.\r\n"
    )]
    public class FactCurrencyRateCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(FactCurrencyRate) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum FactCurrencyRateP {
        __invalid,
        [PKType(Description = "Called 'CurrencyKey' in original sample database")]
        DimCurrencyId,
        [PKType(Type = typeof(DateTime), Description = "Called 'DateKey' in original sample database")]
        Date,
        AverageRate,
        EndOfDayRate,
        // Unnecessary duplicate.
        // Date
    }
}
