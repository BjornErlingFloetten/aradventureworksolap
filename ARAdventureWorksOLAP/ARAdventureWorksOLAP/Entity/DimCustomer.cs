﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a Customer. See also -" + nameof(DimCustomerP) + "- and -" + nameof(DimCustomerCollection) + "-.")]
    public class DimCustomer : PExact<DimCustomerP> {
        public DimCustomer() : base(capacity: 28) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Adds together Title, LastName, FirstName, MiddleName\r\n" +
            "Returns TRUE if finds one of those.\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetName(out IP retval, out string errorResponse) {
            var sb = new System.Text.StringBuilder();
            new List<DimCustomerP> { DimCustomerP.Title, DimCustomerP.LastName, DimCustomerP.FirstName, DimCustomerP.MiddleName }.ForEach(p => {
                if (IP.TryGetPV<string>(p, out var v)) {
                    if (sb.Length > 0) sb.Append(", ");
                    sb.Append(v);
                }
            });
            if (sb.Length == 0) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "None of Title, LastName, FirstName, MiddleName found.";
                return false;
            }
            retval = new PValue<string>(sb.ToString());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Returns -" + nameof(DimGeographyP.PostalCode) + "- through -" + nameof(DimCustomerP.DimGeographyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetPostalCode(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimCustomerP.DimGeographyId), foreignField: PK.FromEnum(DimGeographyP.PostalCode), out retval, out errorResponse);

        [ClassMember(Description =
            "Returns -" + nameof(DimGeographyP.City) + "- through -" + nameof(DimCustomerP.DimGeographyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCity(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimCustomerP.DimGeographyId), foreignField: PK.FromEnum(DimGeographyP.City), out retval, out errorResponse);

        [ClassMember(Description =
            "Returns -" + nameof(DimGeographyP.StateOrProvinceName) + "- through -" + nameof(DimCustomerP.DimGeographyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetStateOrProvince(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimCustomerP.DimGeographyId), foreignField: PK.FromEnum(DimGeographyP.StateOrProvinceName), out retval, out errorResponse);
        [ClassMember(Description = "Returns result from -" + nameof(TryGetStateOrProvince) + "-.")]
        public bool TryGetState(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) => TryGetStateOrProvince(dataStorage, thisKey, out retval, out errorResponse);
        [ClassMember(Description = "Returns result from -" + nameof(TryGetStateOrProvince) + "-.")]
        public bool TryGetProvince(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) => TryGetStateOrProvince(dataStorage, thisKey, out retval, out errorResponse);

        [ClassMember(Description =
            "Returns -" + nameof(DimGeographyP.EnglishCountryName) + "- through -" + nameof(DimCustomerP.DimGeographyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCountry(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimCustomerP.DimGeographyId), foreignField: PK.FromEnum(DimGeographyP.EnglishCountryName), out retval, out errorResponse);
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(DimCustomerP.DimGeographyId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(DimCustomer) + "- instances.\r\n"
    )]
    public class DimCustomerCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(DimCustomer) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
   )]
    public enum DimCustomerP {
        __invalid,
        [PKType(Description = "Called 'GeographyKey' in original sample database")]
        DimGeographyId,
        [PKType(Description = "Called 'CustomerAlternateKey' in original sample database")]
        AlternateKey,
        Title,
        FirstName,
        MiddleName,
        LastName,
        NameStyle,
        BirthDate,
        MaritalStatus,
        Suffix,
        Gender,
        EmailAddress,
        YearlyIncome,
        TotalChildren,
        NumberChildrenAtHome,
        EnglishEducation,
        SpanishEducation,
        FrenchEducation,
        EnglishOccupation,
        SpanishOccupation,
        FrenchOccupation,
        HouseOwnerFlag,
        NumberCarsOwned,
        AddressLine1,
        AddressLine2,
        Phone,
        DateFirstPurchase,
        CommuteDistance,
    }
}
