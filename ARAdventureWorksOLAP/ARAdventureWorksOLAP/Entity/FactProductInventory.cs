﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a change in Product inventory. See also -" + nameof(FactProductInventoryP) + "- and -" + nameof(FactProductInventoryCollection) + "-.")]
    public class FactProductInventory : PExact<FactProductInventoryP> {
        public FactProductInventory() : base(capacity: 7) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
    }
    
    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(FactProductInventoryP.DimProductId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactProductInventory) + "- instances.\r\n"
    )]
    public class FactProductInventoryCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(FactProductInventory) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum FactProductInventoryP {
        __invalid,
        [PKType(Description = "Called 'ProductKey' in original sample database")]
        DimProductId,
        [PKType(Type = typeof(DateTime), Description = "Called 'DateKey' in original sample database")]
        Date,
        [PKType(Type = typeof(DateTime))]
        MovementDate,
        UnitCost,
        UnitsIn,
        UnitsOut,
        UnitsBalance
    }
}
