﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents an Account. See also -" + nameof(DimAccountP) + "- and -" + nameof(DimAccountCollection) + "-.")]
    public class DimAccount : PExact<DimAccountP> {
        public DimAccount() : base(capacity: 8) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(DimAccountP.ParentDimAccountId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactResellerSale) + "- instances.\r\n"
    )]
    public class DimAccountCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(DimAccount) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
   )]
    public enum DimAccountP {
        __invalid,
        [PKType(Description = "Called 'ParentAccountKey' in original sample database")]
        [PKRel(ForeignEntity = typeof(DimAccount))]
        ParentDimAccountId,
        [PKType(Description = "Called 'AccountCodeAlternateKey' in original sample database")]
        AlternateKey,
        [PKType(Description = "Called 'ParentAccountCodeAlternateKey' in original sample database")]
        ParentAlternateKey,
        [PKType(Description = "Called 'AccountDescription' in original sample database")]
        Description,
        [PKType(Description = "Called 'AccountType' in original sample database")]
        Type,
        Operator,
        // One record 98/9530 with value '[Account].[Accounts].[Account Level 04].&[50]/[Account].[Accounts].[Account Level 02].&[97]'
        // Else empty. Deleted.
        // CustomMembers,
        ValueType,
        CustomMemberOptions
    }
}
