﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a SalesTerritory. See also -" + nameof(DimSalesTerritoryP) + "-.")]
    public class DimSalesTerritory : PExact<DimSalesTerritoryP> {
        public DimSalesTerritory() : base(capacity: 5) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Adds together -" + nameof(DimSalesTerritoryP.Country) + "- and -" + nameof(DimSalesTerritoryP.Region) + "- " +
            "(leaves out Region if equivalent to country).\r\n" +
            "Gives a unique logical representative name of every Sales territory.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetName(out IP retval, out string errorResponse) {
            var country = IP.GetPV<string>(DimSalesTerritoryP.Country, "[UNKNOWN COUNTRY]");
            var region = IP.GetPV<string>(DimSalesTerritoryP.Region, "[UNKNOWN REGION]");
            retval = new PValue<string>(country + (region.Equals(country) ? "" : (", " + region)));
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
			return true;
        }
    }

    // Unnecessary, no foreign keys in this collection / table
    //public class DimSalesTerritoryCollection : PCollection {
    //}

    [Enum(
        Description = "Describes class -" + nameof(DimSalesTerritory) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimSalesTerritoryP {
        __invalid,
        /// Not needed / not used. See instead <see cref="DimSalesTerritory.TryGetName"/>
        //[PKType(Description = "Called 'SalesTerritoryAlternateKey' in original sample database")]
        //AlternateKey,
        [PKType(Description = "Called 'SalesTerritoryRegion' in original sample database")]
        Region,
        [PKType(Description = "Called 'SalesTerritoryCountry' in original sample database")]
        Country,
        [PKType(Description = "Called 'SalesTerritoryGroup' in original sample database")]
        Group,
        [PKType(Description = "Called 'SalesTerritoryImage' in original sample database")]
        [PKHTML(Encoder = typeof(PictureEncoder))]
        Image
    }
}