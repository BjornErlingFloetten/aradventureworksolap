﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a Product. See also -" + nameof(DimProductP) + "- and -" + nameof(DimProductCollection) + "-.")]
    public class DimProduct : PExact<DimProductP> {
        public DimProduct() : base(capacity: 35) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Returns " + nameof(DimProductP.EnglishDescription) + ".\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetDescription(out IP retval, out string errorResponse) => IP.TryGetP(DimProductP.EnglishDescription, out retval, out errorResponse);

        [ClassMember(Description =
            "Returns " + nameof(DimProductP.EnglishName) + ".\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetName(out IP retval, out string errorResponse) => IP.TryGetP(DimProductP.EnglishName, out retval, out errorResponse);

        [ClassMember(Description =
            "Returns DimProductSubcategory.DimProductCategory.Name through -" + nameof(DimProductP.DimProductSubcategoryId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCategory(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimProductP.DimProductSubcategoryId),
                foreignField: IKString.FromString(nameof(DimProductCategory) + "." + nameof(DimProductCategoryP.EnglishName)), 
                out retval, out errorResponse);

        [ClassMember(Description =
            "Returns -" + nameof(DimProductSubcategoryP.EnglishName) + "- through -" + nameof(DimProductP.DimProductSubcategoryId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetSubcategory(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimProductP.DimProductSubcategoryId), foreignField: PK.FromEnum(DimProductSubcategoryP.EnglishName), out retval, out errorResponse);
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(DimProductP.DimProductSubcategoryId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(DimProduct) + "- instances.\r\n"
    )]
    public class DimProductCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(DimProduct) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimProductP {
        __invalid,
        [PKType(Description = "Called 'ProductAlternateKey' in original sample database")]
        AlternateKey,
        [PKType(Description = "Called 'ProductSubcategoryKey' in original sample database")]
        DimProductSubcategoryId,
        WeightUnitMeasureCode,
        SizeUnitMeasureCode,
        [PKType(Description = "Called 'EnglishProductName' in original sample database")]
        EnglishName,
        [PKType(Description = "Called 'SpanishProductName' in original sample database")]
        SpanishName,
        [PKType(Description = "Called 'FrenchProductName' in original sample database")]
        FrenchName,
        StandardCost,
        FinishedGoodsFlag,
        Color,
        SafetyStockLevel,
        ReorderPoint,
        ListPrice,
        Size,
        SizeRange,
        Weight,
        DaysToManufacture,
        ProductLine,
        DealerPrice,
        Class,
        Style,
        ModelName,
        [PKType(Description = "Called 'LargePhoto' in original sample database")]
        [PKHTML(Encoder = typeof(PictureEncoder))]
        Photo,
        EnglishDescription,
        FrenchDescription,
        ChineseDescription,
        ArabicDescription,
        HebrewDescription,
        ThaiDescription,
        GermanDescription,
        JapaneseDescription,
        TurkishDescription,
        StartDate,
        EndDate,
        Status,
    }
}
