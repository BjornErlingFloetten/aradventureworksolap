﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents part of an organizational structure. See also -" + nameof(DimOrganizationP) + "- and -" + nameof(DimOrganizationCollection) + "-.")]
    public class DimOrganization : PExact<DimOrganizationP> {
        public DimOrganization() : base(capacity: 4) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign keys like " +
        "-" + nameof(DimOrganizationP.ParentDimOrganizationId) + "- and " +
        "-" + nameof(DimOrganizationP.DimCurrencyId) + "-, " +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactFinance) + "- instances.\r\n"
    )]
    public class DimOrganizationCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(DimOrganization) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimOrganizationP {
        __invalid,
        [PKType(Description = "Called 'ParentOrganizationKey' in original sample database")]
        [PKRel(ForeignEntity = typeof(DimOrganization))]
        ParentDimOrganizationId,
        PercentageOfOwnership,
        [PKType(Description = "Called 'OrganizationName' in original sample database")]
        Name,
        [PKType(Description = "Called 'CurrencyKey' in original sample database")]
        DimCurrencyId
    }
}
