﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a Promotion. See also -" + nameof(DimPromotionP) + "-.")]
    public class DimPromotion : PExact<DimPromotionP> {
        public DimPromotion() : base(capacity: 15) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Returns " + nameof(DimPromotionP.EnglishName) + ".\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetName(out IP retval, out string errorResponse) => IP.TryGetP(DimPromotionP.EnglishName, out retval, out errorResponse);

        [ClassMember(Description =
            "Returns " + nameof(DimPromotionP.EnglishType) + ".\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetType(out IP retval, out string errorResponse) => IP.TryGetP(DimPromotionP.EnglishType, out retval, out errorResponse);

        [ClassMember(Description =
            "Returns " + nameof(DimPromotionP.EnglishCategory) + ".\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCategory(out IP retval, out string errorResponse) => IP.TryGetP(DimPromotionP.EnglishCategory, out retval, out errorResponse);
    }

    // Unnecessary, no foreign keys in this collection / table
    //public class DimPromotionCollection : PCollection {
    //}

    [Enum(
        Description = "Describes class -" + nameof(DimPromotion) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimPromotionP {
        __invalid,
        [PKType(Description = "Called 'PromotionAlternateKey' in original sample database")]
        AlternateKey,
        [PKType(Description = "Called 'EnglishPromotionName' in original sample database")]
        EnglishName,
        [PKType(Description = "Called 'SpanishPromotionName' in original sample database")]
        SpanishName,
        [PKType(Description = "Called 'FrenchPromotionName' in original sample database")]
        FrenchName,
        DiscountPct,
        [PKType(Description = "Called 'EnglishPromotionType' in original sample database")]
        EnglishType,
        [PKType(Description = "Called 'SpanishPromotionType' in original sample database")]
        SpanishType,
        [PKType(Description = "Called 'FrenchPromotionType' in original sample database")]
        FrenchType,
        [PKType(Description = "Called 'EnglishPromotionCategory' in original sample database")]
        EnglishCategory,
        [PKType(Description = "Called 'SpanishPromotionCategory' in original sample database")]
        SpanishCategory,
        [PKType(Description = "Called 'FrenchPromotionCategory' in original sample database")]
        FrenchCategory,
        StartDate,
        EndDate,
        MinQty,
        MaxQty
    }
}
