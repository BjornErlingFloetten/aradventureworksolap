﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP
{

    [Class(Description = "Represents an Employee. See also -" + nameof(DimEmployeeP) + "- and -" + nameof(DimEmployeeCollection) + "-.")]
    public class DimEmployee : PExact<DimEmployeeP>
    {
        public DimEmployee() : base(capacity: 28) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Adds together Title, LastName, FirstName, MiddleName\r\n" +
            "Returns TRUE if finds one of those.\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetName(out IP retval, out string errorResponse)
        {
            var sb = new System.Text.StringBuilder();
            new List<DimEmployeeP> { DimEmployeeP.Title, DimEmployeeP.LastName, DimEmployeeP.FirstName, DimEmployeeP.MiddleName }.ForEach(p =>
            {
                if (IP.TryGetPV<string>(p, out var v))
                {
                    if (sb.Length > 0) sb.Append(", ");
                    sb.Append(v);
                }
            });
            if (sb.Length == 0)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
				errorResponse = "None of Title, LastName, FirstName, MiddleName found.";
                return false;
            }
            retval = new PValue<string>(sb.ToString());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
			return true;
        }
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign keys " +
        "-" + nameof(DimEmployeeP.ParentEmployeeId) + "- and " +
        "-" + nameof(DimEmployeeP.DimSalesTerritoryId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(DimEmployee) + "- instances.\r\n"
    )]
    public class DimEmployeeCollection : PCollection
    {
    }

    [Enum(
        Description = "Describes class -" + nameof(DimEmployee) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimEmployeeP
    {
        __invalid,
        [PKType(Description = "Called 'ParentEmployeeKey' in original sample database")]
        [PKRel(ForeignEntity = typeof(DimEmployee), OppositeTerm = "ChildEmployee")]
        ParentEmployeeId,
        EmployeeNationalIDAlternateKey,
        ParentEmployeeNationalIDAlternateKey,
        [PKType(Description = "Called 'SalesTerritoryKey' in original sample database")]
        DimSalesTerritoryId,
        FirstName,
        LastName,
        MiddleName,
        // Was always 0, deleted
        // NameStyle,
        Title,
        HireDate,
        BirthDate,
        LoginID,
        EmailAddress,
        Phone,
        MaritalStatus,
        EmergencyContactName,
        EmergencyContactPhone,
        [PKType(Description = "Note: All instances of 'SalariedFlag = 0' has been filtered out at import")]
        SalariedFlag,
        Gender,
        PayFrequency,
        BaseRate,
        VacationHours,
        SickLeaveHours,
        // Was always 0, deleted
        // CurrentFlag,
        [PKType(Description = "Note: All instances of 'SalesPersonFlag = 0' has been filtered out at import")]
        SalesPersonFlag,
        [PKType(Description = "Called 'DepartmentName' in original sample database")]
        Department,
        StartDate,
        EndDate,
        Status,
        [PKType(Description = "Called 'EmployeePhoto' in original sample database")]
        [PKHTML(Encoder = typeof(PictureEncoder))]
        Photo
    }
}
