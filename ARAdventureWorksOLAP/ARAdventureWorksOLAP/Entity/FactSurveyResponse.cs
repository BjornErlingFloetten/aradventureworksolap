﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a Survey response. See also -" + nameof(FactSurveyResponseP) + "- and -" + nameof(FactSurveyResponseCollection) + "-.")]
    public class FactSurveyResponse : PExact<FactSurveyResponseP> {
        public FactSurveyResponse() : base(capacity: 6) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
    }
    
    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign keys " +
        "-" + nameof(FactSurveyResponseP.DimCustomerId) + "-, " +
        "-" + nameof(FactSurveyResponseP.DimProductCategoryId) + "- and " +
        "-" + nameof(FactSurveyResponseP.DimProductSubcategoryId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactSurveyResponse) + "- instances.\r\n"
    )]
    public class FactSurveyResponseCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(FactSurveyResponse) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum FactSurveyResponseP {
        __invalid,
        [PKType(Description = "Called 'DateKey' in original sample database")]
        Date,
        [PKType(Description = "Called 'CustomerKey' in original sample database")]
        DimCustomerId,
        [PKType(Description = "Called 'ProductCategoryKey' in original sample database")]
        DimProductCategoryId,
        EnglishProductCategoryName,
        [PKType(Description = "Called 'ProductSubcategoryKey' in original sample database")]
        DimProductSubcategoryId,
        EnglishProductSubcategoryName
        // Unnecessary duplicate.
        // Date
    }
}
