﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a Reseller sale. See also -" + nameof(FactResellerSaleP) + "- and -" + nameof(FactResellerSaleCollection) + "-.")]
    public class FactResellerSale : PExact<FactResellerSaleP> {
        public FactResellerSale() : base(capacity: 24) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        // Positive result, the values differ in some cases.\
        //  public bool TryGetTest1(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) {
        //      retval = new PValue<double>(IP.GetPV(FactResellerSaleP.ExtendedAmount, 0d) - IP.GetPV(FactResellerSaleP.SalesAmount, 0d));
        //      errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //return true;
        //  }
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign keys like " +
        "-" + nameof(FactResellerSaleP.DimProductId) + "-, " +
        "-" + nameof(FactResellerSaleP.DimResellerId) + "-, " +
        "-" + nameof(FactResellerSaleP.DimPromotionId) + "-, " +
        "-" + nameof(FactResellerSaleP.DimEmployeeId) + "- and " +
        "-" + nameof(FactResellerSaleP.DimSalesTerritoryId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactResellerSale) + "- instances.\r\n"
    )]
    public class FactResellerSaleCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(FactResellerSale) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum FactResellerSaleP {
        __invalid,
        [PKType(Description = "Called 'ProductKey' in original sample database")]
        DimProductId,
        [PKType(Type = typeof(DateTime), Description = "Called 'OrderDateKey' in original sample database")]
        OrderDate,
        [PKType(Type = typeof(DateTime), Description = "Called 'DueDateKey' in original sample database")]
        DueDate,
        [PKType(Type = typeof(DateTime), Description = "Called 'ShipDateKey' in original sample database")]
        ShipDate,
        [PKType(Description = "Called 'ResellerKey' in original sample database")]
        DimResellerId,
        [PKType(Description = "Called 'EmployeeKey' in original sample database")]
        DimEmployeeId,
        [PKType(Description = "Called 'PromotionKey' in original sample database. Note: All instances of 'DimPromotionId = 1' (No discount) has been filtered out at import")]
        DimPromotionId,
        [PKType(Description = "Called 'CurrencyKey' in original sample database")]
        DimCurrencyId,
        [PKType(Description = "Called 'SalesTerritoryKey' in original sample database")]
        DimSalesTerritoryId,
        SalesOrderNumber,
        SalesOrderLineNumber,
        RevisionNumber,
        OrderQuantity,
        [PKType(Type = typeof(double))]
        UnitPrice,
        [PKType(Type = typeof(double))]
        ExtendedAmount,
        [PKType(Type = typeof(double), Description = "Note: All instances of 'UnitPriceDiscountPct = 0' has been filtered out at import")]
        UnitPriceDiscountPct,
        [PKType(Type = typeof(double), Description = "Note: All instances of 'DiscountAmount = 0' has been filtered out at import")]
        DiscountAmount,
        [PKType(Type = typeof(double))]
        ProductStandardCost,
        [PKType(Type = typeof(double))]
        TotalProductCost,
        [PKType(Type = typeof(double))]
        SalesAmount,
        [PKType(Type = typeof(double))]
        TaxAmt,
        [PKType(Type = typeof(double))]
        Freight,
        CarrierTrackingNumber,
        CustomerPONumber
        // Unnecessary duplicates.
        // OrderDate
        // DueDate
        // ShipDate
    }
}
