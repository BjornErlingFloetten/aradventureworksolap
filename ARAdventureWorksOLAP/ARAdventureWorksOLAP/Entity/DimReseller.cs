﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents a Reseller. See also -" + nameof(DimResellerP) + "- and -" + nameof(DimResellerCollection) + "-.")]
    public class DimReseller : PExact<DimResellerP> {
        public DimReseller() : base(capacity: 19) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Returns -" + nameof(DimGeographyP.PostalCode) + "- through -" + nameof(DimResellerP.DimGeographyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetPostalCode(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimResellerP.DimGeographyId), foreignField: PK.FromEnum(DimGeographyP.PostalCode), out retval, out errorResponse);

        [ClassMember(Description =
            "Returns -" + nameof(DimGeographyP.City) + "- through -" + nameof(DimResellerP.DimGeographyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCity(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimResellerP.DimGeographyId), foreignField: PK.FromEnum(DimGeographyP.City), out retval, out errorResponse);

        [ClassMember(Description =
            "Returns -" + nameof(DimGeographyP.StateOrProvinceName) + "- through -" + nameof(DimResellerP.DimGeographyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetStateOrProvince(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimResellerP.DimGeographyId), foreignField: PK.FromEnum(DimGeographyP.StateOrProvinceName), out retval, out errorResponse);
        [ClassMember(Description = "Returns result from -" + nameof(TryGetStateOrProvince) + "-.")]
        public bool TryGetState(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) => TryGetStateOrProvince(dataStorage, thisKey, out retval, out errorResponse);
        [ClassMember(Description = "Returns result from -" + nameof(TryGetStateOrProvince) + "-.")]
        public bool TryGetProvince(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) => TryGetStateOrProvince(dataStorage, thisKey, out retval, out errorResponse);

        [ClassMember(Description =
            "Returns -" + nameof(DimGeographyP.EnglishCountryName) + "- through -" + nameof(DimResellerP.DimGeographyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCountry(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(DimResellerP.DimGeographyId), foreignField: PK.FromEnum(DimGeographyP.EnglishCountryName), out retval, out errorResponse);
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(DimResellerP.DimGeographyId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(DimReseller) + "- instances.\r\n"
    )]
    public class DimResellerCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(DimReseller) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimResellerP {
        __invalid,
        [PKType(Description = "Called 'GeographyKey' in original sample database")]
        DimGeographyId,
        ResellerAlternateKey,
        Phone,
        BusinessType,
        [PKType(Description = "Called 'ResellerName' in original sample database")]
        Name,
        NumberEmployees,
        OrderFrequency,
        OrderMonth,
        FirstOrderYear,
        LastOrderYear,
        ProductLine,
        AddressLine1,
        AddressLine2,
        AnnualSales,
        BankName,
        MinPaymentType,
        MinPaymentAmount,
        AnnualRevenue,
        YearOpened
    }
}
