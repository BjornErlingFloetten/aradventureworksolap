﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

	[Class(Description = "Represents a CallCenter. See also -" + nameof(FactCallCenterP) + "-.")]
	public class FactCallCenter : PExact<FactCallCenterP> {
		public FactCallCenter() : base(capacity: 12) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
	}

	// Unnecessary, no foreign keys in this collection / table
	//public class FactCallCenterCollection : PCollection {
	//}

	[Enum(
		Description = "Describes class -" + nameof(FactCallCenter) + "-.",
		AREnumType = AREnumType.PropertyKeyEnum
	)]
	public enum FactCallCenterP {
		__invalid,
		[PKType(Type=typeof(DateTime), Description = "Called 'DateKey' in original sample database")]
		Date,
		WageType,
		Shift,
		LevelOneOperators,
		LevelTwoOperators,
		TotalOperators,
		Calls,
		AutomaticResponses,
		Orders,
		IssuesRaised,
		AverageTimePerIssue,
		ServiceGrade
		// Unnecessary duplicate
		// Date
	}
}
