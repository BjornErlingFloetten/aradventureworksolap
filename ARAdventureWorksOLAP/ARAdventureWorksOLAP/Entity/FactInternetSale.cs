﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description = "Represents an Internet sale. See also -" + nameof(FactInternetSaleP) + "- and -" + nameof(FactInternetSaleCollection) + "-.")]
    public class FactInternetSale : PExact<FactInternetSaleP> {
        public FactInternetSale() : base(capacity: 15) { } // There is a slight performance improvement at application initialization by stating capacity explicit now

        [ClassMember(Description =
            "Returns -" + nameof(DimCurrencyP.AlternateKey) + "- through -" + nameof(FactInternetSaleP.DimCurrencyId) + "- with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCurrency(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(FactInternetSaleP.DimCurrencyId), foreignField: PK.FromEnum(DimCurrencyP.AlternateKey), out retval, out errorResponse);

        [ClassMember(Description =
            "Returns DimCustomer.DimGeography.DimSalesTerritory.Name with the help of -" + nameof(EntityMethodKey.TryGetForeignField) + "-.\r\n" +
            "\r\n" +
            "Gives an example of why -" + nameof(FactInternetSaleP.DimSalesTerritoryId) + "- is superfluous.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetSalesTerritory(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) =>
            EntityMethodKey.TryGetForeignField(dataStorage, this, foreignKey: PK.FromEnum(FactInternetSaleP.DimCustomerId), foreignField: IKString.FromString("DimGeography.DimSalesTerritory.Name"), out retval, out errorResponse);

        //public bool TryGetTest1(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) {
        //    retval = new PValue<double>(IP.GetPV(FactInternetSaleP.ExtendedAmount, 0d) - IP.GetPV(FactInternetSaleP.SalesAmount, 0d));
        //    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //    return true;
        //}

        //public bool TryGetTest2(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) {
        //    retval = new PValue<double>(IP.GetPV(FactInternetSaleP.ProductStandardCost, 0d) - IP.GetPV(FactInternetSaleP.TotalProductCost, 0d));
        //    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //    return true;
        //}

        //public bool TryGetTest3(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) {
        //    retval = new PValue<double>(IP.GetPV(FactInternetSaleP.UnitPrice, 0d) - IP.GetPV(FactInternetSaleP.SalesAmount, 0d));
        //    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //    return true;
        //}

    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign keys like " +
        "-" + nameof(FactInternetSaleP.DimCustomerId) + "-, " +
        "-" + nameof(FactInternetSaleP.DimPromotionId) + "-, " +
        "-" + nameof(FactInternetSaleP.DimProductId) + "- and " +
        "-" + nameof(FactInternetSaleP.DimCurrencyId) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactInternetSale) + "- instances.\r\n"
    )]
    public class FactInternetSaleCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(FactInternetSale) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum FactInternetSaleP {
        __invalid,
        [PKType(Description = "Called 'ProductKey' in original sample database")]
        DimProductId,
        [PKType(Type = typeof(DateTime), Description = "Called 'OrderDateKey' in original sample database (link to DimDate)")]
        OrderDate,
        [PKType(Type = typeof(DateTime), Description = "Called 'DueDateKey' in original sample database (link to DimDate)")]
        DueDate,
        [PKType(Type = typeof(DateTime), Description = "Called 'ShipDateKey' in original sample database  (link to DimDate)")]
        ShipDate,
        [PKType(Description = "Called 'CustomerKey' in original sample database")]
        DimCustomerId,
        [PKType(Description = "Called 'PromotionKey' in original sample database. Note: All instances of 'DimPromotionId = 1' (No discount) has been filtered out at import")]
        DimPromotionId,
        [PKType(Description = "Called 'CurrencyKey' in original sample database")]
        DimCurrencyId,
        [PKType(Description =
            "Called 'SalesTerritoryKey' in original sample database.\r\n" +
            "\r\n" +
            "Note: This key is  really not needed because we can traverse DimCustomerId.DimGeography.DimSalesTerritory " +
            "like -" + nameof(FactInternetSale.TryGetSalesTerritory) + "-."
        )]
        DimSalesTerritoryId,
        [PKType(Description = "-" + nameof(SalesOrderNumber) + "- plus -" + nameof(SalesOrderLineNumber) + "- constitutes primary key of -" + nameof(FactInternetSale) + "-")]
        SalesOrderNumber,
        [PKType(Description = "-" + nameof(SalesOrderNumber) + "- plus -" + nameof(SalesOrderLineNumber) + "- constitutes primary key of -" + nameof(FactInternetSale) + "-")]
        SalesOrderLineNumber,
        // Was always 1, deleted
        // RevisionNumber,
        // Was always 1, deleted
        // OrderQuantity,
        // Was always equal to SalesAmount, deleted
        // UnitPrice,
        // Always equal to SalesAmount, deleted
        // ExtendedAmount,
        // Was always 0, deleted
        // UnitPriceDiscountPct,
        // Was always 0, deleted
        // DiscountAmount,
        // Was always equal to TotalProductCost, deleted
        // ProductStandardCost,
        [PKType(Type = typeof(double))]
        TotalProductCost,
        [PKType(Type = typeof(double))]
        SalesAmount,
        [PKType(Type = typeof(double))]
        TaxAmt,
        [PKType(Type = typeof(double))]
        Freight,
        CarrierTrackingNumber,
        CustomerPONumber,
        // Unnecessary duplicates.
        //[PKType(Description = "Called 'OrderDate' in original sample database", Type = typeof(DateTime))]
        //Order2Date,
        //[PKType(Description = "Called 'DueDate' in original sample database", Type = typeof(DateTime))]
        //Due2Date,
        //[PKType(Description = "Called 'ShipDate' in original sample database", Type = typeof(DateTime))]
        //Ship2Date
    }
}
