﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description =
        "Represents a Department. See also -" + nameof(DimDepartmentP) + "- and -" + nameof(DimDepartmentCollection) + "-.\r\n" +
        "\r\n" +
        "Called 'DimDepartmentGroup' in original sample database"
    )]
    public class DimDepartment : PExact<DimDepartmentP> {
        public DimDepartment() : base(capacity: 2) { } // There is a slight performance improvement at application initialization by stating capacity explicit now
    }

    [Class(Description =
        "The existence of this class ensures that -" + nameof(ARConcepts.Indexing) + "- will be used for foreign key -" + nameof(DimDepartmentP.ParentDimDepartmentId) + "-." +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will use this class as container class for all -" + nameof(FactFinance) + "- instances.\r\n"
    )]
    public class DimDepartmentCollection : PCollection {
    }

    [Enum(
        Description = "Describes class -" + nameof(DimDepartment) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DimDepartmentP {
        __invalid,
        [PKType(Description = "Called 'ParentDepartmentGroupKey' in original sample database")]
        ParentDimDepartmentId,
        [PKType(Description = "Called 'DepartmentGroupName' in original sample database")]
        Name,
    }
}
