﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP
{

    [Class(Description =
        "Contains the user friendly welcome text (front page) for the application.\r\n" +
        "\r\n" +
        "An instance of this class is used as root-element in -" + nameof(ARCAPI.DataStorage.Storage) + "-.\r\n" +
        "Used in order for -" + nameof(ToHTMLSimpleSingle) + "- to override the default rather useless listing of API internal information."
    )]
    public class Root : PRich
    {

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Contains hand coded HTML introducing ARAdventureWorks.\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null) =>
            "<h1>ARAdventureWorks</h1>\r\n" +
            "<p>ARAdventureWorks is a demonstration of <a href=\"https://bitbucket.org/BjornErlingFloetten/ARCore\">AgoRapide (ARCore)</a> " +
            "with data from Microsoft's AdventureWorks example dataset.</p>\r\n" +
            "<p>An online example of this application is found at <a href=\"http://ARAdventureWorksOLAP.AgoRapide.com\">http://ARAdventureWorksOLAP.AgoRapide.com</a></p>\r\n" +
            "\r\n" +
            (
                /// Information from <see cref="PropertyStreamLine.StoreFailure"/>
                /// TODO: Link this better together, create separate class called "ParseOrStoreFailure"
                !Program.DataStorage.Storage.TryGetP<IP>("ParseOrStoreFailure", out var f) ? "" : (
                    "<p style=\"color:red\">ERROR: " +
                    "Some -" + nameof(ARConcepts.PropertyStream) + "- content did not parse correctly. " +
                    "Details: <a href=\"ParseOrStoreFailure\">ParseOrStoreFailure</a></p>\r\n"
            )) +
            "\r\n" +
            "<h2>Some sample queries</h2>\r\n" +
            "<p>" +
            "Note how queries, although predefined here, do not have to be. " +
            "You can execute any query directly on-the-fly in your browser as long as you can construct the correct URL for it." +
            "</p>\r\n" +
            "<p>For more sample queries, see also <a href=\"http://ARNorthwind.AgoRapide.com\">http://ARNorthwind.AgoRapide.com</a></p>\r\n" +
            (!Program.DataStorage.Storage.GetPV<bool>(StreamProcessorP.TimestampIsOld, defaultValue: false) ? "" : ( // Note that absence of key is considered as FALSE
                "<p style=\"color:red\">" +
                    "API is currently initializing. Queries may not return complete result.\r\n" +
                    "TODO: Implement new -" + nameof(PSPrefix.dtr) + "- format (introduced in Feb 2022) for quicker initialization.\r\n" +
                "</p>\r\n"
            )) +
            "<table><tr><th>Explanation</th><th>URL</th></tr>\r\n" +
            string.Join("", new List<(string url, string text)> {
                /// TODO: <see cref="ARConcepts.LinkInsertionInDocumentation"/> currently not possible here because would get link in link-text
                /// TODO: Use both Title and Description, and have links in Description instead
                ("DimCustomer/SELECT Name, PostalCode, City, StateOrProvince, Country/ORDER BY City", "Customer with addresses. Shows use of " + nameof(EntityMethodKey) + " through methods like " + nameof(DimCustomer.TryGetCity)),
                ("DimProduct/WHERE Photo NEQ Null/SELECT Name, ModelName, Description, Photo", "Products for which we have a photo"),
                ("DimProduct/WHERE Photo EQ Null/SELECT Name, ModelName, Description/ORDER BY Name", "Products without a photo"),
                ("DimEmployee/SELECT Name, ParentEmployee.Name, ChildEmployee.Name.Count() AS DirectSubordinates/ORDER BY DirectSubordinates DESC/THEN BY Name", "Employees with Supervisors and Subordinates"),
                ("DimEmployee/WHERE Status = 'Current'/SELECT Name, BaseRate, Status, Department, Photo/ORDER BY BaseRate","Employees sorted by pay"),
                ("DimEmployee/WHERE Status = 'Current'/AGGREGATE Department/ORDER BY _SUM DESC","Count of employees per department"),
                ("FactInternetSale/SELECT OrderDate, SalesAmount, DimCurrency.AlternateKey, DimProduct.Name, DimSalesTerritory.Name/ORDER BY OrderDate DESC/SKIP 0/TAKE 100","What we are currently selling on the Internet"),
                ("FactInternetSale/PIVOT OrderDate.YearMonth() BY DimCurrency.AlternateKey SUM SalesAmount/SELECT *.TMB() AS */ORDER BY OrderDate.YearMonth()Id DESC","Internet sales per year and month"),
                ("FactInternetSale/SELECT OrderDate, OrderDate.AWFiscalYear(), SalesAmount, DimCustomer.Name/ORDER BY OrderDate DESC", "Internet sales with Fiscal year"),
                ("FactInternetSale/WHERE DimCurrency.AlternateKey = 'AUD'/PIVOT DimCustomer.Name0x002B','0x002BDimCustomerId BY OrderDate.Year() SUM SalesAmount/ORDER BY 2020 DESC/TAKE 1000", "Top 1000 Australian Internet customers for 2020"),
                ("FactInternetSale/SELECT SalesTerritory, DimCustomer.DimGeography.DimSalesTerritory.Name, DimSalesTerritory.Name/SKIP 0/TAKE 1000","Demonstrate how foreign key DimSalesTerritoryId is not really needed in FactInternetSales"),
                ("FactResellerSale/PIVOT DimSalesTerritory.Name0x002B'_'0x002BDimCurrency.Name BY OrderDate.Year() SUM SalesAmount","Reseller sales pr Sales territory country and year (note inclusion of currency for 'safety' reasons)"),
                ("FactResellerSale/PIVOT DimSalesTerritory.Name0x002B'_'0x002BDimCurrency.Name BY OrderDate.Year() SUM SalesAmount","Reseller sales per Sales territory and year (note inclusion of currency for 'safety' reasons)"),
                ("FactResellerSale/PIVOT OrderDate.YearMonth() BY DimCurrency.AlternateKey SUM SalesAmount/SELECT *.TMB() AS */ORDER BY OrderDate.YearMonth()Id DESC","Reseller sales per year and month"),
                ("FactResellerSale/PIVOT DimReseller.FirstOrderYear BY DimReseller.ProductLine/ORDER BY DimReseller.FirstOrderYearId DESC","Count of reseller sales by Reseller's first order year"),
                ("FactResellerSale/WHERE DimCurrency.AlternateKey EQ 'EUR'/PIVOT DimReseller.FirstOrderYear BY DimReseller.ProductLine SUM SalesAmount/SELECT *.TMB() AS */ORDER BY DimReseller.FirstOrderYearId DESC", "SalesAmount of reseller sales done in EUR by Reseller's first order year"),
                ("FactResellerSale/WHERE DimCurrency.AlternateKey EQ 'CAD'/PIVOT OrderDate.Year() BY DimReseller.ProductLine SUM SalesAmount/SELECT *.TMB() AS */ORDER BY OrderDate.Year()Id DESC","SalesAmount of reseller sales done in CAD by Reseller's ProductLine"),
                ("SELECT CountP AS NumberOfEntities, CountPRec AS NumberOfPropertiesOfEntities, ToPropertyStream.Length() AS SizeInDatabase/ORDER BY NumberOfEntities DESC", "Database statistics (Note: Slow query)")
            }.Select(tuple =>
                "<tr><td>" +
                "<a href=\"" + nameof(PSPrefix.dt) + "/" + tuple.url + "/TITLE " + tuple.text + "\">" +
                System.Net.WebUtility.HtmlEncode(tuple.text) +
                "</a>" +
                "</td><td>" +
                System.Net.WebUtility.HtmlEncode(tuple.url).Replace("/", "<br>") +
                "</td></tr>\r\n"
            )) +
            "</table>\r\n" +
            "\r\n" +
            "<h2>Database entities</h2>\r\n" +
            (!Program.DataStorage.Storage.GetPV<bool>(StreamProcessorP.TimestampIsOld, defaultValue: false) ? "" : ( // Note that absence of key is considered as FALSE
                "<p style=\"color:red\">" +
                    "API is currently initializing. Entity tables have not been completely populated yet. Refresh this page for update.\r\n" +
                "</p>\r\n"
            )) +
            "<table><tr><th>Entity</th><th>Count</th></tr>\r\n" +
            string.Join("", Program.DataStorage.Storage[nameof(PSPrefix.dt)].
                Where(ikip =>
                    !nameof(StreamProcessorP.TimestampIsOld).Equals(ikip.Key.ToString()) &&
                    !nameof(StreamProcessorP.Timestamp).Equals(ikip.Key.ToString())
                ).
                OrderBy(ikip => ikip.Key.ToString()).
                Select(ikip =>
                    "<tr><td>" +
                    "<a href=\"" + nameof(PSPrefix.dt) + "/" + System.Net.WebUtility.UrlEncode(ikip.Key.ToString()) + "/All\">" +
                    System.Net.WebUtility.HtmlEncode(ikip.Key.ToString()) +
                    "</a>" +
                    "</td><td align=right>" +
                    Program.DataStorage.Storage[nameof(PSPrefix.dt)].GetP<IP>(ikip.Key).Count() +
                    "</td></tr>\r\n"
                )
             ) +
            "</table>\r\n" +
            "\r\n" +
            "<h2>Miscellaneous links</h2>\r\n" +
            string.Join("", new List<(string url, string text)> {
                (nameof(PSPrefix.doc),"AgoRapide (ARCore) documentation"),
                (nameof(PSPrefix.app),"Internal application state")
            }.Select(tuple =>
                "<p><a href=\"" + System.Net.WebUtility.UrlEncode(tuple.url) + "\">" +
                System.Net.WebUtility.HtmlEncode(tuple.text) + "</a></p>\r\n"
            )) +
            "\r\n" +
            "<h2>Other information</h2>\r\n" +
            "<p>Author: Bjørn Erling Fløtten, Trondheim, Norway. bef_adventureworks@bef.no</p>\r\n" +
            "<p>The date and time values in the example database are modified at application startup in order to look fresh, " +
            "and in order for queries like <a href=\"dt/FactInternetSale/WHERE OrderDate = Today\">today's Internet sales</a> to function.</p>\r\n" +
            "<p>" +
            "Database original SQL dump was of size 160 MB.<br>\r\n" +
            "The AgoRapide -" + nameof(ARConcepts.PropertyStream) + "- conversion of the same data is about 172 MB.<br>\r\n" +
            "In-memory footprint of this application (without any optimizations) is around 240 MB (Baseline memory snapshot done in Visual Studio's Performance Profiler).<br>\r\n" +
            "<br>\r\n" +
            "Note: Table -" + nameof(FactProductInventory) + "- has been omitted for practical reasons. " +
            "It will approximately more than double the conversion size / RAM use.<br>\r\n" +
            "(See C# source code, -" + nameof(Converter) + "-, for instructions about how to include it.)<br>\r\n" +
            "</p>\r\n" +
            "<p>" +
            "The original database has been extensively improved with relation to field names, unused fields and unnecessary values.<br>\r\n" +
            "Field names have been shortened, like 'DimProductCategory.EnglishName' instead of 'DimProductCategory.ProductCategoryEnglishName'.<br>\r\n" +
            "Fields with the same value everywhere have simply been deleted, like 'DimInternetSales.RevisionNumber' which was always '1'.<br>\r\n" +
            "For fields with a predominantly '0'-value, that value is not included ('0' being replaced with NULL). Example: 'DimEmployeeP.SalesPersonFlag'.<br>\r\n" +
            "<br>\r\n" +
            "Also, unneccessary tables are not included, like 'DimDate' which is replaced with the concept of -" + nameof(FunctionKey) + "- " +
            "(see for instance -" + nameof(FunctionKeyAWFiscalYear) + "-).<br>\r\n" +
            "Table 'DimScenario' has been replaced with -" + nameof(Scenario) + "-.<br>\r\n" +
            "Other tables not included are 'NewFactCurrencyRate', 'ProspectiveBuyer', 'sysdiagrams', 'DatabaseLog'.<br>\r\n" +
            "<br>\r\n" +
            "Many strategic -" + nameof(EntityMethodKey) + "- has been implemented, like -" + nameof(DimCustomer.TryGetName) + "-.<br>\r\n" +
            "<br>\r\n" +
            "All these improvements radically simplifies the process of browsing and making queries (doing OLAP on the data).<br>\r\n" +
            "</p>" +
            "<p>\r\n" +
            "Note that the data in the database is somewhat 'random' in nature, it does not look natural " +
            "(it gives the impression of being artificially generated). " +
            "It is therefore difficult to show some natural insights that can be gathered by doing OLAP on the data, " +
            "so this application mostly demonstrates technical aspects of AgoRapide.<br>\r\n" +
            "Note: The author welcomes access to a real representative database where som new insights can be made with the help of AgoRapide as an OLAP tool.\r\n" +
            "</p>\r\n" +
            "<p>The AdventureWorks database which have been used as the base for ARAdventureWorks can be found at<br>\r\n" +
            "https://github.com/microsoft/sql-server-samples/blob/master/samples/databases/adventure-works/data-warehouse-install-script <br>\r\n" +
            "with the following license:<br>\r\n" +
            "https://github.com/microsoft/sql-server-samples/blob/master/license.txt <br>\r\n" +
            "</p>\r\n";
    }
}