﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using ARCQuery;

namespace ARAdventureWorksOLAP {

    [Class(Description =
        "Extracts Adventure Works' Fiscal Year from date field.\r\n" +
        "\r\n" +
        "The Fiscal Year goes from 1st of July to 30th of June.\r\n" +
        "\r\n" +
        "Note: Use of -" + nameof(FunctionKey) +"- in AgoRapide lessens the need for separate tables with derived values.\r\n " +
        "In ARAdventureWorkOLAP for instance, the DimDate-table of the original database has been omitted because it can be replaced with " +
        "the -" + nameof(FunctionKey) + "- concept\r\n" +
        "\r\n" +
        "See also -" + nameof(Program.Main) + "- which calls -" + nameof(FunctionKey.AddParser) + "- for -" + nameof(FunctionKeyAWFiscalYear) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyAWFiscalYear : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'AWFiscalYear()'\r\n" +
            "Examples:\r\n" +
            "'FactInternetSales/SELECT OrderDate, OrderDate.AWFiscalYear(), SalesAmount, DimCustomer.Name',\r\n" +
            "'Order/PIVOT Created.AWFiscalYear() BY xx()'."
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionRel));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<int>(dateTime.Month <= 6 ? dateTime.Year - 1 : dateTime.Year), out retval, out errorResponse);

        public new static FunctionKeyAWFiscalYear Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyAWFiscalYearException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyAWFiscalYear retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyAWFiscalYear retval, out string errorResponse) =>
            TryParseSingleWord(value, "awfiscalyear", () => new FunctionKeyAWFiscalYear(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "AWFiscalYear()";
        public class InvalidFunctionKeyAWFiscalYearException : ApplicationException {
            public InvalidFunctionKeyAWFiscalYearException(string message) : base(message) { }
            public InvalidFunctionKeyAWFiscalYearException(string message, Exception inner) : base(message, inner) { }
        }
    }
}