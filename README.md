# AgoRapide 2020
ARAdventureWorksOLAP is a demonstration of the query language in AgoRapide 2020, a library for building data-oriented backend applications written with .NET Core / Standard. 

An online instance of ARAdventureWorksOLAP can be found [here](http://ARAdventureWorksOLAP.AgoRapide.com).
The main page demonstrates various queries that can be performed.
(Note: First time loading may be a bit slow because of application being initialized. After that queries are quite fast.)

For AgoRapide 2020 in general, see its Bitbucket repository [ARCore](https://bitbucket.org/BjornErlingFloetten/ARCore).

In order to compile and run ARAdventureWorksOLAP locally, ARCore must be cloned in a parallell folder to ARAdventureWorksOLAP, like git/ARCore and git/ARAdventureWorksOLAP.

See also [ARNorthwind](https://bitbucket.org/BjornErlingFloetten/ARNorthwind) which operates on a much smaller dataset.

Please feel free to contact the author at bef at bef dot no for more information.